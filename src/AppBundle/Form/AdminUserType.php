<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AdminUserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', array('label' => 'Username'))
            ->add('password', 'password', array('label' => 'Password'))
            ->add('email', 'text', array('label' => 'Email'))
            ->add('rolesName', 'choice', 
                array(
                    'label' => 'User Type',
                    'choices' => array(
                        'ROLE_ADMIN' => 'Admin',
                        'ROLE_USER' => 'Guest'
                    ),
                    'mapped' => false
                )
            )
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\AdminUser'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_adminuser';
    }
}
