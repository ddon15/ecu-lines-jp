<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
// use Symfony\Component\Form\Extension\Core\Type\ChoiceType;  

class ScheduleType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('scheduleType','choice', 
                array(
                'label' => 'Type', 
                'choices' => array(
                    'Import' => 'import',
                    'Export' => 'export'
                ),
                'choices_as_values' => true,
                'expanded' => true,
                'multiple' => false,
                'label_attr' => array(
                    'class' => 'radio-inline'
                )
            ))
            ->add('name', 'text', array('label' => 'Name'))
            ->add('scheduleMonth', 'choice', 
                array(
                    'label' => 'Month',
                    'choices' => array(
                        'January' => 'January',
                        'February' => 'Febuary',
                        'March' => 'March',
                        'April' => 'April',
                        'May' => 'May',
                        'June' => 'June',
                        'July' => 'July',
                        'August' => 'August',
                        'September' => 'September',
                        'October' => 'October',
                        'November' => 'November',
                        'December' => 'December'
                    )
                )
            )
            ->add('scheduleYear', 'choice', 
                array(
                    'label' => 'Year',
                    'choices' => array(
                        '2015' => '2015',
                        '2016' => '2016'
                    )
                )
            )
            ->add('entryType','choice',
                array(
                    'label' => 'Type of Entry',
                    'choices' => array(
                        'Goods' => 'goods',
                        'Dangerous' => 'dangerous'
                    ),
                    'choices_as_values' => true,
                    'expanded' => true,
                    'multiple' => false,
                    'label_attr' => array(
                        'class' => 'radio-inline'
                    )
                )
            )
            // ->add('continent', 'entity', 
            //     array(
            //     'label' => 'Continent/Region', 
            //     'required' => true,
            //     'expanded' => false,
            //     'class' => 'AppBundle:Continent',
            //     'choice_label' => 'name',
            //     'multiple' => false,
            //     'mapped' => true
            // ))
            ->add('portOrigin', "entity", 
                array(
                'label' => 'Port Origin', 
                'required' => true,
                'expanded' => false,
                'class' => 'AppBundle:Port',
                'multiple' => false,
                'choice_label' => 'name',        
                'mapped' => true,
                'placeholder' => 'Select Port Origin'
            ))
            ->add('portDestination', "entity", 
                array(
                'label' => 'Port Destination', 
                'required' => true,
                'expanded' => false,
                'class' => 'AppBundle:Port',
                'multiple' => false,
                'choice_label' => 'name',  
                'mapped' => true,
                'placeholder' => 'Select Port Destination'
             
            ))
            ->add('excelFile', 'file', array('label' => 'Excel File', 'data_class' => NULL))
            ->add('pdfFile', 'file', array('label' => 'PDF File', 'data_class' => NULL))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Schedule'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_schedule';
    }
}
