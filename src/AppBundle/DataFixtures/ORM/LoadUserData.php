<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\AdminUser;

class LoadUserData implements FixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        var_dump('getting container here');
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
      $user = new AdminUser();
      $user->setUsername("admin");
      $user->setSalt(md5(uniqid()));
      $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
      $user->setPassword($encoder->encodePassword('admin', $user->getSalt()));
      $user->setStatus(1);
      $user->setEmail('admin@eculines.com');

      $user2 = new AdminUser();
      $user2->setUsername("diovannie");
      $user2->setSalt(md5(uniqid()));
      $user2->setPassword($encoder->encodePassword('diovannie', $user2->getSalt()));
      $user2->setStatus(1);
      $user2->setEmail('diovannie@eculines.com');

      $manager->persist($user);
      $manager->persist($user2);

      $manager->flush();
    }
}
