<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use AppBundle\Entity\Continent;
use AppBundle\Entity\Country;
use AppBundle\Entity\Port;

class LoadContinentPortData implements FixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        var_dump('getting container here');
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $continents = array(
            "Asia" => [
                "Bangladesh" => ["Chittagong"],             
                "Brunei" => ["Muara"],              
                "Cambodia" => ["Phnom Penh", "Sihanoukville"],              
                "Hongkong" => ["Hongkong"],             
                "Indonesia" => ["Jakarta", "Semarang", "Surabaya"],
                "India" =>["Bangalore", "Calcutta", "New Delhi","Chennai","Nhava Sheva","Mumbai"], 
                "Japan" => ["Tokyo", "Yokohama", "Osaka", "Kobe" , "Osaka" , "Nagoya", "Shimizu", "Shimizu", "Moji", "Hakata", "Horishima", "Naha", "ISHIKARI/TOMAKOMAI/SENDAI/NIIGATA/TOYAMA/KANAZAWA"],             
                "Malaysia" => ["Penang", "Port Kelang", "Pasir Gudang"],                
                "Myanmar" => ["Yangon"],                
                "Pakistan" => ["Karachi"],
                "Philippines" => ["Cebu", "Manila-South", "Manila-North", "Davao"],                 
                "Singapore" => ["Singapore"],               
                "Singapore" => ["Singapore"],               
                "South Korea" => ["Busan", "Incheon"],              
                "Sri Lanka" => ["Colombo"],             
                "Taiwan" => ["Kaohsiung", "Keelung", "Taichung"],               
                "Thailand" => ["Bangkok", "Laem Chabang", "Lat Krabang"],               
                "Vietnam" => ["Danang", "Haipong", "Cat Lai - Ho Chi Minh"],               
            ],
            "China" => [
                "China" =>["Dalian - Liaoning", "Nanjing", "Xiamen", "Shanghai", "Dalian", "Qingdao", "Ningbo", "Yantai", "Weihai", "Huangpu - Ji Si", "Huangpu - Sui Gang", "Huangpu - Wu Chong", "Jiaoxin", "Macau", "Fuzhou", "Shantou","Wuhan", "Xingang", "Zhongshan", "Zhuhai"],  
            ],
            "Europe" => [
                "Belgium" => ["Antwerp"],           
                "Bulgaria" => ["Varna"],         
                "Czech" => ["Prague"],          
                "Denmark" => ["Aarhus", "Copenhagen"],          
                "Finland" => ["Helsinki"],          
                "France" => ["Le Havre", "Fos Sur Mer"],            
                "Germany" => ["Hamburg"],           
                "Ireland" => ["Dublin"],            
                "Italy" => ["Milano", "Genoa"],         
                "Netherlands" => ["Rotterdam"],         
                "Poland" => ["Gdynia"],         
                "Russia" => ["Vladivostok"],            
                "Slovenia" => ["Koper"],            
                "Spain" => ["Barcelona"],           
                "Sweden" => ["Gothenburg"],         
                "United Kingdom" => ["Southampton", "Felixstowe"],          
            ],
            "USA" => [
                "USA" =>["Atlanta - Georgia", "Baltimore - Missouri", "Boston - Massachusetts", "Buffalo - New York", "Charleston - South Carolina", "Charlotte -  North Carolina", "Chattanooga - Tennessee", "Chicago - Illinois", "Cincinnati - Ohio", "Dallas - Texas", "Dayton - Ohio", "Denver - Colorado", "Detroit - Michigan", "El paso - Texas", "Grand Rapids - Michigan", "Greensboro - North Carolina", "Greenville - South Carolina", "Hidalgo - Texas", "Houston - Texas", "Indianapolis - Indiana", "Jacksonville - Florida", "Kansas City - Missouri", "Knoxville Tennessee", "Laredo - Texas", "Los Angeles", "Memphis - Tennessee", "Miami - Florida", "Milwaukee - Wisconsin", "Minneapolis - Minnesota", "Nashville - Tennessee", "New Orleans - Louisiana", "New York", "Nogales - Arizona", "Norfolk - Virginia", "Oakland - California", "Orlando - Florida", "Philadelphia - Pennsylvania", "Pittsburgh - Pennsylvania", "Portland - Oregon", "Raleigh - NC", "Richmond - Virginia", "Salt Lake City - Utah", "San Antonio, Texas", "San Diego - California", "San Francisco", "Savannah - Georgia", "Seattle - Washington", "Shreveport - Louisiana", "Springfield - Illinois", "Saint Louis", "Tacoma - Washington", "Tampa - Florida", "Toledo - Ohio", "Wilmington - Delaware", "Honolulu"],
            ],
            "Mediterranean" => [
                "Israel" => ["Ashdod"],
                "Greece" => ["Piraeus"],
                "Egypt" => ["Alexandria"],
                "Turkey" => ["Istanbul"],
                "Cyprus" => ["Limassol"],

            ],
            "Canada" => [
                "Canada" => ["Toronto", "Vancouver", "Montreal"],
            ],
            "Latin America" =>[
                "Colombia" =>["Buenaventura"], 
                "Argentina" => ["Buenos Aires"], 
                "Peru" =>["Callao"], 
                "Panama" => ["Colon Free Zone"], 
                "Ecuador" => ["Guayaquil"], 
                "Mexico" => ["Manzanillo"], 
                "Brazil" => ["Rio de Janeiro", "Santos"], 
                "Chile" => ["Valparaiso"],
            ],
            "Africa" => [
                "Egypt" =>["Alexandria"], 
                "South Aftrica" =>["Cape Town","Durban","Johannesburg", "Port Elizabeth"], 
                "Nigeria" =>["Lagos"],
                "Kenya" => ["Mombasa"], 
                "Ghana" => ["Tema"],
            ],
            "Middle East" => [
                "Lebanon" => ["Beirut"],   
                "United Arab Emirates" => ["Dubai"], 
                "Pakistan" => ["Karachi"], 

            ],
            "Oceania" => [
                "Australia" => ["Adelaide", "Brisbane", "Fremantle", "Melbourne", "Sydney"], 
                "New Zealand" => ["Christchurch", "Auckland"],
                "Guam" => ["Guam"], 
            ]
        );

        foreach ($continents as $continentName => $countries) {
            $continent = new Continent();
            $continent->setName($continentName);
            $manager->persist($continent);
            foreach ($countries as $countryName => $ports) {
                $country = new Country();
                $country->setName($countryName);
                $country->setContinent($continent);
                $manager->persist($country);
                foreach ($ports as $portName) {
                    $port = new Port();
                    $port->setName($portName);
                    $port->setCountry($country);
                    $manager->persist($port);  
                }   
            }

            $manager->flush();
            $manager->clear();
        }      
    }
}
