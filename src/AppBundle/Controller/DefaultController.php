<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $data = array();
        $data['portSchedules'] = $this->getDoctrine()->getRepository('AppBundle:Schedule')->getLatestPortSchedules(5);
        $data['schedules'] = $this->getDoctrine()->getRepository('AppBundle:Schedule')->findAll();
        $data['isClient'] = 1;
        $data['isHome'] = 1;
        $data['portOrigin'] = '';

        if ($data['portSchedules'])
            $data['portOrigin'] = $data['portSchedules'][0]->getPortOrigin()->getName();

        $data['countryPorts'] = array();
        $japanCountry = $this->getDoctrine()->getRepository('AppBundle:Country')->findOneByName('Japan');
        $data['ports'] = $this->getDoctrine()->getRepository('AppBundle:Port')->getAllPort();
        $data['japanPorts'] = array();
        $data['baseUrl'] = $this->generateUrl('homepage');
        
        if ($japanCountry)
            $data['japanPorts'] = $this->getDoctrine()->getRepository('AppBundle:Port')->findByCountryId($japanCountry->getId());
        
        $data['countries'] = $this->getDoctrine()->getRepository('AppBundle:Country')->getAllCountry();
        
        return $this->render('client/homepage.html.twig', $data);
    }

    public function schedulesAction($portOriginId, $countryId, $portDestinationId)
    {
    	$data = array();
        
        $queryParams = array(
            'portOriginId' => $portOriginId,
            'countryId' => $countryId,
            'portDestinationId' => $portDestinationId
        );
        
        $data['message'] = "Select appropriate port information to be search.";
        $data['isClient'] = 1;
        $data['countryPorts'] = array();
        $japanCountry = $this->getDoctrine()->getRepository('AppBundle:Country')->findOneByName('Japan');
        $data['ports'] = $this->getDoctrine()->getRepository('AppBundle:Port')->getAllPort();
        $data['japanPorts'] = array();

        if ($countryId) 
            $data['countryPorts'] = $this->getDoctrine()->getRepository('AppBundle:Port')->findByCountryId($countryId); 
        
        if ($japanCountry)
            $data['japanPorts'] = $this->getDoctrine()->getRepository('AppBundle:Port')->findByCountryId($japanCountry->getId());
        
        $data['countries'] = $this->getDoctrine()->getRepository('AppBundle:Country')->getAllCountry();

        if(!$portOriginId && !$countryId && !$portDestinationId)
            $data['schedules'] = $this->getDoctrine()->getRepository('AppBundle:Schedule')->getLatestPortSchedules(12);
        else
            $data['schedules'] = $this->getDoctrine()->getRepository('AppBundle:Schedule')->getScheduleByParams($queryParams);
        
        if (!$data['schedules'] && ($portDestinationId || $countryId) || $portOriginId)
            $data['message'] = "Schedule(s) not found.";

        if ($data['schedules'])
            $data['message'] = '';
        
        $data['queryParams'] = $queryParams;
        $data['baseUrl'] = $this->generateUrl('homepage');
        
        return $this->render('client/schedule.html.twig', $data);
    }

    public function scheduleListAction($category, $fileType)
    {
        $data = array();
        $data['schedules'] = array();
        $data['fileTypes'] = array('pdf' => 'pdf', 'excel' => 'excel');

        $queryParams = array(
            'portOriginId' => '',
            'countryId' => '',
            'portDestinationId' => '',
            'category' => $category
        );

        if ($category) 
            $data['schedules'] = $this->getDoctrine()->getRepository('AppBundle:Schedule')->getScheduleByParams($queryParams);
       
        $data['fileType'] = $fileType;

        return $this->render('client/scheduleList.html.twig', $data);

    }
    public function surchageAction()
    {
        $data['countryPorts'] = array();
        $data['isClient'] = 1;
         $data['isHome'] = 1;
        $japanCountry = $this->getDoctrine()->getRepository('AppBundle:Country')->findOneByName('Japan');
        $data['ports'] = $this->getDoctrine()->getRepository('AppBundle:Port')->getAllPort();
        $data['japanPorts'] = array();
        $data['baseUrl'] = $this->generateUrl('homepage');
        
        if ($japanCountry)
            $data['japanPorts'] = $this->getDoctrine()->getRepository('AppBundle:Port')->findByCountryId($japanCountry->getId());
        
        $data['countries'] = $this->getDoctrine()->getRepository('AppBundle:Country')->getAllCountry();
        return $this->render('client/surchage.html.twig', $data);
    }

    public function aboutUsAction()
    {
        $data['countryPorts'] = array();
        $data['isClient'] = 1;
         $data['isHome'] = 1;
        $japanCountry = $this->getDoctrine()->getRepository('AppBundle:Country')->findOneByName('Japan');
        $data['ports'] = $this->getDoctrine()->getRepository('AppBundle:Port')->getAllPort();
        $data['japanPorts'] = array();
        $data['baseUrl'] = $this->generateUrl('homepage');
        
        if ($japanCountry)
            $data['japanPorts'] = $this->getDoctrine()->getRepository('AppBundle:Port')->findByCountryId($japanCountry->getId());
        
        $data['countries'] = $this->getDoctrine()->getRepository('AppBundle:Country')->getAllCountry();
        return $this->render('client/aboutus.html.twig', $data);
    }

    public function generateSearchUrl()
    {
        $request = $this->getRequest();
        
    }

    public function privacyStatementAction()
    {
        return $this->render('client/privacypolicy.html.twig');
    }

    public function termsOfUseAction()
    {
        return $this->render('client/terms.html.twig');
    }
}

