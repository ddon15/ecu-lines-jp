<?php

namespace AppBundle\Controller\Api;

use FOS\RestBundle\Controller\FOSRestController;

class CountryController extends FOSRestController
{
    public function getCountryPortsAction($countryId)
    {   
        $data = array();

        $data['ports'] = $this->getDoctrine()->getRepository('AppBundle:Port')->findByCountryId($countryId);
        
        $view = $this->view($data, 200);

        return $this->handleView($view);
    }

    //TOdo move
    public function generateSearchUrlAction()
    {	
    	$data = array();
    	$request = $this->getRequest();
    	$queryParams = $request->request->all();

    	$data['url'] = $this->generateUrl('schedules', $queryParams);

    	$view = $this->view($data, 200);

    	return $this->handleView($view);
    }
}