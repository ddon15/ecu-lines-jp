<?php

namespace AppBundle\Controller\Backend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class UserController extends Controller
{
    public function indexAction()
    {
    	$data = array();

        $request = $this->getRequest();
        $query = $this->getDoctrine()->getRepository('AppBundle:AdminUser')->findAll();

        $data['users'] = $query;
        $data['tabUser'] = 1;

        return $this->render('backend/User/users.html.twig', $data);
    }

    public function addAction()
    {
	    $data = array();
		$request = $this->getRequest();
		$user = new \AppBundle\Entity\AdminUser();
	    $form = $this->createForm(new \AppBundle\Form\AdminUserType(), $user);
	    $form->handleRequest($request);

	    if ($form->isValid() && $form->isSubmitted()) {

	        $formData = $form->getData();
	        $role = $request->request->all()['appbundle_adminuser']['rolesName']; //todo: 

	       	$user->setUsername($formData->getUsername());
	       	$user->setSalt(md5(uniqid()));
	       	$encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
	       	$user->setPassword($encoder->encodePassword($formData->getPassword(), $user->getSalt()));
	       	$user->setStatus(1);
	       	$user->setEmail($formData->getEmail());
	       
	       	$user->setRoles($role);

	        $entityManager = $this->getDoctrine()->getManager();
	        $entityManager->persist($user);
	        $entityManager->flush();
	        

	        return $this->redirectToRoute('admin_users');
	    }

	    $data['tabUser'] = 1;
	    $data['form'] = $form->createView();

		return $this->render('backend/User/new.html.twig', $data);
    }

    public function editAction($id)
    {
    	    $data = array();
    		$request = $this->getRequest();
    		$user = $this->getDoctrine()->getRepository('AppBundle:AdminUser')->find($id);
    	    $form = $this->createForm(new \AppBundle\Form\AdminUserType(), $user);
    	    $form->handleRequest($request);

    	    if ($form->isValid() && $form->isSubmitted()) {

    	        $formData = $form->getData();
    	        $role = $request->request->all()['appbundle_adminuser']['rolesName']; //todo:
    	        
    	       	$user->setUsername($formData->getUsername());
    	       	$user->setSalt(md5(uniqid()));
    	       	$encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
    	       	$user->setPassword($encoder->encodePassword($formData->getPassword(), $user->getSalt()));
    	       	$user->setStatus(1);
    	       	$user->setEmail($formData->getEmail());
    	       	$user->setRoles($role);

    	        $entityManager = $this->getDoctrine()->getManager();
    	        $entityManager->persist($user);
    	        $entityManager->flush();
    	        

    	        return $this->redirectToRoute('admin_users');
    	    }

    	    $data['tabUser'] = 1;
    	    $data['form'] = $form->createView();

    		return $this->render('backend/User/edit.html.twig', $data);
    }

    public function deleteAction()
    {
    	$em = $this->getDoctrine()->getEntityManager();
    	$entite = $em->getRepository('AppBundle:Admin')->find($id);
    	$em->remove($entite);
    	$em->flush();

    	return $this->redirectToRoute('admin_users');
    }
}
