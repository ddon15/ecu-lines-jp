<?php

namespace AppBundle\Controller\Backend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Schedule;
use AppBundle\Entity\Continent;
use AppBundle\Entity\Port;
use AppBundle\Form\ScheduleType;

class ScheduleController extends Controller
{
    public function indexAction()
    {
        $data = array();

        $request = $this->getRequest();
        $query = $this->getDoctrine()->getRepository('AppBundle:Schedule')->findAll();

        $data['schedules'] = $query;
        $data['tabSchedule'] = 1;

        return $this->render('backend/Schedule/list.html.twig', $data);
    }

    public function newAction()
    {
        $data = array();
    	$request = $this->getRequest();
    	$schedule = new Schedule();
        $form = $this->createForm(new ScheduleType(), $schedule);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {

            $formData = $form->getData();
            // $file stores the uploaded PDF file
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $excelFile = $schedule->getExcelFile();
            $pdfFile = $schedule->getPdfFile();

            // // Generate a unique name for the file before saving it
            $filename = str_replace(' ', '_', $formData->getName()) . '_' . $formData->getScheduleMonth() .'_'. $formData->getScheduleYear();
            $excelFilename = $filename.'.'.$excelFile->guessExtension();
            $pdfFilename = $filename.'.'.$pdfFile->guessExtension();

            // Move the file to the directory where brochures are stored
            $scheduleFileDir = $this->container->getParameter('kernel.root_dir').'/../web/uploads/schedules';
            $excelFile->move($scheduleFileDir.'/excel', $excelFilename);
            $pdfFile->move($scheduleFileDir.'/pdf', $pdfFilename);

            $country = $formData->getPortOrigin()->getCountry();

            $continent = $country->getContinent();
            
            $schedule->setExcelFile($excelFilename);
            $schedule->setPdfFile($pdfFilename);
            $schedule->setContinent($continent);
            $schedule->setPortOrigin($formData->getPortOrigin());
            $schedule->setPortDestination($formData->getPortDestination());
            $schedule->setScheduleType($formData->getScheduleType());
            $schedule->setEntryType($formData->getEntryType());
            $schedule->setName($formData->getName());
            $schedule->setScheduleMonth($formData->getScheduleMonth());
            $schedule->setScheduleYear($formData->getScheduleYear());
            $schedule->setAdmin($this->getUser());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($schedule);
            $entityManager->flush();
            

            return $this->redirectToRoute('admin_schedules');
        }

        $data['tabSchedule'] = 1;
        $data['form'] = $form->createView();

    	return $this->render('backend/Schedule/new.html.twig', $data);
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $entite = $em->getRepository('AppBundle:Schedule')->find($id);
        $em->remove($entite);
        $em->flush();

        return $this->redirectToRoute('admin_schedules');
    }

    public function editAction($id)
    {
        $data = array();
        $request = $this->getRequest();
        $schedule = $this->getDoctrine()->getRepository('AppBundle:Schedule')->find($id);
        $form = $this->createForm(new ScheduleType(), $schedule);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {

            $formData = $form->getData();
            // $file stores the uploaded PDF file
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $excelFile = $schedule->getExcelFile();
            $pdfFile = $schedule->getPdfFile();

            // // Generate a unique name for the file before saving it
            $filename = str_replace(' ', '_', $formData->getName()) . '_' . $formData->getScheduleMonth() .'_'. $formData->getScheduleYear();
            $excelFilename = $filename.'.'.$excelFile->guessExtension();
            $pdfFilename = $filename.'.'.$pdfFile->guessExtension();

            // Move the file to the directory where brochures are stored
            $scheduleFileDir = $this->container->getParameter('kernel.root_dir').'/../web/uploads/schedules';
            $excelFile->move($scheduleFileDir.'/excel', $excelFilename);
            $pdfFile->move($scheduleFileDir.'/pdf', $pdfFilename);

            $country = $formData->getPortOrigin()->getCountry();

            $continent = $country->getContinent();
            
            $schedule->setExcelFile($excelFilename);
            $schedule->setPdfFile($pdfFilename);
            $schedule->setContinent($continent);
            $schedule->setPortOrigin($formData->getPortOrigin());
            $schedule->setPortDestination($formData->getPortDestination());
            $schedule->setScheduleType($formData->getScheduleType());
            $schedule->setEntryType($formData->getEntryType());
            $schedule->setName($formData->getName());
            $schedule->setScheduleMonth($formData->getScheduleMonth());
            $schedule->setScheduleYear($formData->getScheduleYear());
            $schedule->setAdmin($this->getUser());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($schedule);
            $entityManager->flush();
            

            return $this->redirectToRoute('admin_schedules');
        }

        $data['tabSchedule'] = 1;
        $data['form'] = $form->createView();

        return $this->render('backend/Schedule/edit.html.twig', $data);   
    }
}
