<?php

namespace AppBundle\Controller\Backend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DashboardController extends Controller
{
    public function indexAction()
    {
    	$data = array();

        $request = $this->getRequest();
        $query = $this->getDoctrine()->getRepository('AppBundle:Schedule')->findAll();

        $data['schedules'] = $query;
        $data['tabDashboard'] = 1;
        return $this->render('backend/dashboard.html.twig', $data);
    }

    public function surchargeAction()
    {
		$data = array();

		$data['message'] = '';
	    $request = $this->getRequest();

	    if ($request->isMethod('POST')) {
	    	
            // Move the file to the directory where brochures are stored
            $scheduleFileDir = $this->container->getParameter('kernel.root_dir').'/../web/uploads/surcharge/';
            
            move_uploaded_file($_FILES['surcharge']['tmp_name'], $scheduleFileDir . 'surcharge.pdf');

           	$error = $_FILES['surcharge']["error"];
		
			if($error == UPLOAD_ERR_OK)
            	$data['message'] = 'Successfully updated surcharge pdf file.';
	    }
	    
	    $data['tabSurcharge'] = 1;
	    return $this->render('backend/surcharge.html.twig', $data);
    }
}
