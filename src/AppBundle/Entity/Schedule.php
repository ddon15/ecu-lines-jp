<?php

namespace AppBundle\Entity;

/**
 * Schedule
 */
class Schedule
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $scheduleType;

    /**
     * @var string
     */
    private $entryType;

    /**
     * @var string
     */
    private $scheduleMonth;

    /**
     * @var string
     */
    private $scheduleYear;

    /**
     * @var string
     */
    private $pdfFile;

    /**
     * @var string
     */
    private $excelFile;

    /**
     * @var \DateTime
     */
    private $dateCreated;

    /**
     * @var \DateTime
     */
    private $dateModified;

    /**
     * @var \AppBundle\Entity\Continent
     */
    private $continent;

    /**
     * @var \AppBundle\Entity\Port
     */
    private $portOrigin;

    /**
     * @var \AppBundle\Entity\Port
     */
    private $portDestination;

    /**
     * @var \AppBundle\Entity\AdminUser
     */
    private $admin;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Schedule
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set scheduleType
     *
     * @param string $scheduleType
     *
     * @return Schedule
     */
    public function setScheduleType($scheduleType)
    {
        $this->scheduleType = $scheduleType;

        return $this;
    }

    /**
     * Get scheduleType
     *
     * @return string
     */
    public function getScheduleType()
    {
        return $this->scheduleType;
    }

    /**
     * Set entryType
     *
     * @param string $entryType
     *
     * @return Schedule
     */
    public function setEntryType($entryType)
    {
        $this->entryType = $entryType;

        return $this;
    }

    /**
     * Get entryType
     *
     * @return string
     */
    public function getEntryType()
    {
        return $this->entryType;
    }

    /**
     * Set scheduleMonth
     *
     * @param string $scheduleMonth
     *
     * @return Schedule
     */
    public function setScheduleMonth($scheduleMonth)
    {
        $this->scheduleMonth = $scheduleMonth;

        return $this;
    }

    /**
     * Get scheduleMonth
     *
     * @return string
     */
    public function getScheduleMonth()
    {
        return $this->scheduleMonth;
    }

    /**
     * Set scheduleYear
     *
     * @param string $scheduleYear
     *
     * @return Schedule
     */
    public function setScheduleYear($scheduleYear)
    {
        $this->scheduleYear = $scheduleYear;

        return $this;
    }

    /**
     * Get scheduleYear
     *
     * @return string
     */
    public function getScheduleYear()
    {
        return $this->scheduleYear;
    }

    /**
     * Set pdfFile
     *
     * @param string $pdfFile
     *
     * @return Schedule
     */
    public function setPdfFile($pdfFile)
    {
        $this->pdfFile = $pdfFile;

        return $this;
    }

    /**
     * Get pdfFile
     *
     * @return string
     */
    public function getPdfFile()
    {
        return $this->pdfFile;
    }

    /**
     * Set excelFile
     *
     * @param string $excelFile
     *
     * @return Schedule
     */
    public function setExcelFile($excelFile)
    {
        $this->excelFile = $excelFile;

        return $this;
    }

    /**
     * Get excelFile
     *
     * @return string
     */
    public function getExcelFile()
    {
        return $this->excelFile;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return Schedule
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dateModified
     *
     * @param \DateTime $dateModified
     *
     * @return Schedule
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;

        return $this;
    }

    /**
     * Get dateModified
     *
     * @return \DateTime
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }

    /**
     * Set continent
     *
     * @param \AppBundle\Entity\Continent $continent
     *
     * @return Schedule
     */
    public function setContinent(\AppBundle\Entity\Continent $continent = null)
    {
        $this->continent = $continent;

        return $this;
    }

    /**
     * Get continent
     *
     * @return \AppBundle\Entity\Continent
     */
    public function getContinent()
    {
        return $this->continent;
    }

    /**
     * Set portOrigin
     *
     * @param \AppBundle\Entity\Port $portOrigin
     *
     * @return Schedule
     */
    public function setPortOrigin(\AppBundle\Entity\Port $portOrigin = null)
    {
        $this->portOrigin = $portOrigin;

        return $this;
    }

    /**
     * Get portOrigin
     *
     * @return \AppBundle\Entity\Port
     */
    public function getPortOrigin()
    {
        return $this->portOrigin;
    }

    /**
     * Set portDestination
     *
     * @param \AppBundle\Entity\Port $portDestination
     *
     * @return Schedule
     */
    public function setPortDestination(\AppBundle\Entity\Port $portDestination = null)
    {
        $this->portDestination = $portDestination;

        return $this;
    }

    /**
     * Get portDestination
     *
     * @return \AppBundle\Entity\Port
     */
    public function getPortDestination()
    {
        return $this->portDestination;
    }

    /**
     * Set admin
     *
     * @param \AppBundle\Entity\AdminUser $admin
     *
     * @return Schedule
     */
    public function setAdmin(\AppBundle\Entity\AdminUser $admin = null)
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * Get admin
     *
     * @return \AppBundle\Entity\AdminUser
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    public function __construct() {
        $this->dateCreated = new \DateTime("now");
        $this->dateModified = new \DateTime("now");
    }

    public function isSamePortValue() {
        return $this->portOrigin !== $this->portDestination;
    }
}
