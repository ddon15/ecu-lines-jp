<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;

class DataRepositoryService {
    
    protected $em;
    private $container;
    
    // Dependency Injection argument params
    public function __construct(EntityManager $entityManager, Container $container) {
        $this->em = $entityManager;
        $this->container = $container;
    }
    
    /**
     * Find an user by role
     *
     * @param string $role
     * @return entities
     */
    public function findUsersByRole($role) {
        $qb = $this->em->createQueryBuilder();
        
         $qb->select('u')
            ->from('AppBundle:AdminUser', 'u')
            ->where('u.roles LIKE :roles')
            ->setParameter('roles', '%"' . $role . '"%');
        
        return $qb->getQuery()->getResult();
    }
    
    /**
     * Find port by country
     *
     * @param int $countryId
     * @return entities
     */
    public function findPortByCountryID($countryId) {
        return $this->getEntityManager()
        ->createQuery(
            'SELECT p, c FROM AppBundle:Port p
            JOIN p.country c
            WHERE c.id =:countryId
            ORDER BY p.name'
        )->setParameter('countryId', $countryId)->getResult();
    }
    
    /**
     * Example of how to retrieve another service within a service.
     * 
     * @PLEASE DELETE; THIS IS JUST AN EXAMPLE
     * @param type $html
     */
    public function createPDF($html = "") {
        $pdf = $this->container->get("test.test");

        $pdf->create('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->generate();
    }
    
}