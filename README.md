ecu
===

A Symfony project created on October 21, 2015, 8:29 am.


Local development setup resources:

http://symfony.com/doc/current/book/installation.html

<!-- Virtual Host -->
<VirtualHost *:80>
        ServerName ecu.local
        DocumentRoot /Applications/MAMP/htdocs/ecu-lines-jp/web
        <Directory /Applications/MAMP/htdocs/ecu-lines-jp/web>
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Order allow,deny
                allow from all
        </Directory>
</VirtualHost>

