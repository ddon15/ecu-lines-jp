<?php include "inc.header2.php";?>
			<section id="maincontent" class="content-body">
				<div class="container">
					<div class="row">
						<div class="col-sm-11 center-block text-center">
							<h1>Support</h1>
						</div>
						<div class="col-sm-11 center-block">
							<div id="accordion2" class="panel-group">
								<div class="panel panel-default">			  
									
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="panel-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#content1" aria-expanded="false">
												<span>How can I delete CUSTOM behaviors and rewards?</span>
											</a>
										</h4>
									</div><!-- /.panel-heading -->
									
									<div id="content1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
										<div class="panel-body">
											<p>You can DELETE CUSTOM BEHAVIORS AND REWARDS by swiping left and tapping the delete button when it appears.</p>
										</div><!-- /.panel-body -->
									</div><!-- /.content -->
									
								</div><!-- /.panel -->
								
								<div class="panel panel-default">			  
									
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="panel-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#content2" aria-expanded="false">
												<span>How many spaces should I use for my child's or student's chart?</span>
											</a>
										</h4>
									</div><!-- /.panel-heading -->
									
									<div id="content2" class="panel-collapse collapse" aria-expanded="false">
										<div class="panel-body">
											<p>We have designed Behavior World to be as flexible as possible. You have the ability to control the amount of spaces to suit the individual needs of your child or student. You may use fewer spaces for more immediate reinforcement. You may use a greater amount of spaces for more delayed reinforcement. It's completely up to you!</p>
										</div><!-- /.panel-body -->
									</div><!-- /.content -->
									
								</div><!-- /.panel -->
								
								<div class="panel panel-default">			  
									
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="panel-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#content3" aria-expanded="false">
												<span>When should I use the timer feature?</span>
											</a>
										</h4>
									</div><!-- /.panel-heading -->
									
									<div id="content3" class="panel-collapse collapse" aria-expanded="false">
										<div class="panel-body">
											<p>The timer is useful in situations where the child is expected to demonstrate a behavior for a specified amount of time.  Examples include five minutes of sustained attention, keeping hands to self for 10 minutes, staying seated for 10 minutes, etc.</p>
										</div><!-- /.panel-body -->
									</div><!-- /.content -->
									
								</div><!-- /.panel -->
								<div class="panel panel-default">			  
									
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="panel-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#content4" aria-expanded="false">
												<span>What should I do if I feel my child or student has not earned a buddy token?</span>
											</a>
										</h4>
									</div><!-- /.panel-heading -->
									
									<div id="content4" class="panel-collapse collapse" aria-expanded="false">
										<div class="panel-body">
											<p>Our goal is to help build child success.  First, consider reducing the amount of target behaviors.  For a child who has a history of behavioral challenges, we recommend focusing on only one behavior initially.  As the child demonstrates consistent improvement in the targeted behavior, you may consider adding one to target.  Secondly, you may make the target behavior more achievable by modifying it as needed.</p>
										</div><!-- /.panel-body -->
									</div><!-- /.content -->
									
								</div><!-- /.panel -->
								<div class="panel panel-default">			  
									
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="panel-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#content5" aria-expanded="false">
												<span>As a teacher, how can I inform parents who do not use email that I am using Behavior World to support their child at school?</span>
											</a>
										</h4>
									</div><!-- /.panel-heading -->
									
									<div id="content5" class="panel-collapse collapse" aria-expanded="false">
										<div class="panel-body">
											<p>We have prepared permission slips that may be sent home for such occasions.</p>
										</div><!-- /.panel-body -->
									</div><!-- /.content -->
									
								</div><!-- /.panel -->
								<div class="panel panel-default">			  
									
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="panel-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#content6" aria-expanded="false">
												<span>How do I reset my “Award Buddy Token” lock PIN/PASSCODE?</span>
											</a>
										</h4>
									</div><!-- /.panel-heading -->
									
									<div id="content6" class="panel-collapse collapse" aria-expanded="false">
										<div class="panel-body">
											<p>Your PIN/PASSCODE can be reset by logging out of your account and then logging back in.  The next time the lock is accessed, you will be prompted to enter a new PIN/PASSCODE.</p>
										</div><!-- /.panel-body -->
									</div><!-- /.content -->
									
								</div><!-- /.panel -->
								<div class="panel panel-default">			  
									
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="panel-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#content7" aria-expanded="false">
												<span>As a teacher, how can I inform parents who do not use email that I am using Behavior World to support their child at school?</span>
											</a>
										</h4>
									</div><!-- /.panel-heading -->
									
									<div id="content7" class="panel-collapse collapse" aria-expanded="false">
										<div class="panel-body">
											<p>We have prepared permission slips that may be sent home for such occasions.</p>
										</div><!-- /.panel-body -->
									</div><!-- /.content -->
									
								</div><!-- /.panel -->
								<div class="panel panel-default">			  
									
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="panel-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#content8" aria-expanded="false">
												<span>Am I able to register both as a Teacher and a Parent? Can I register as parent and an individual working with children?</span>
											</a>
										</h4>
									</div><!-- /.panel-heading -->
									
									<div id="content8" class="panel-collapse collapse" aria-expanded="false">
										<div class="panel-body">
											<p>Absolutely!  Many of us who work with children have children of our own.  As such, we allow each user to create multiple accounts.  You will be prompted to add a different email address and password for each account. Simply log in and out of each account according to your needs.</p>
										</div><!-- /.panel-body -->
									</div><!-- /.content -->
									
								</div><!-- /.panel -->
								<div class="panel panel-default">			  
									
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="panel-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#content9" aria-expanded="false">
												<span>Is Behavior World appropriate for a late-elementary school/middle school-age children? </span>
											</a>
										</h4>
									</div><!-- /.panel-heading -->
									
									<div id="content9" class="panel-collapse collapse" aria-expanded="false">
										<div class="panel-body">
											<p>The app is designed for elementary school-aged children.  However, if a child still demonstrates interest in an avatar (i.e. buddy) and is motivated by our game-like positive reinforcement chart, then by all means use Behavior World to support them.  Adults can select or customize goals and rewards that are appropriate for a given child or class.</p>
										</div><!-- /.panel-body -->
									</div><!-- /.content -->
								</div><!-- /.panel -->
								<div class="panel panel-default">			  
									
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="panel-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#content10" aria-expanded="false">
												<span>Is Behavior World appropriate for a late-elementary school/middle school-age children? </span>
											</a>
										</h4>
									</div><!-- /.panel-heading -->
									
									<div id="content10" class="panel-collapse collapse" aria-expanded="false">
										<div class="panel-body">
											<p>The app is designed for elementary school-aged children.  However, if a child still demonstrates interest in an avatar (i.e. buddy) and is motivated by our game-like positive reinforcement chart, then by all means use Behavior World to support them.  Adults can select or customize goals and rewards that are appropriate for a given child or class.</p>
										</div><!-- /.panel-body -->
									</div><!-- /.content -->
								</div><!-- /.panel -->
								<div class="panel panel-default">			  
									
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="panel-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#content11" aria-expanded="false">
												<span>As a parent, do I create an account under my name or should my child create their own account / on their own device?</span>
											</a>
										</h4>
									</div><!-- /.panel-heading -->
									
									<div id="content11" class="panel-collapse collapse" aria-expanded="false">
										<div class="panel-body">
											<p>Parents first create their own account using their preferred email account and password.  After registering as a parent, you will be prompted to create a profile for a child.</p>
										</div><!-- /.panel-body -->
									</div><!-- /.content -->
								</div><!-- /.panel -->
							</div>
						</div>
					</div><!-- /.row -->
				</div><!-- /.container -->
			</section>
		</main>
<?php include "inc.footer.php";?>
		