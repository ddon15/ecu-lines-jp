					<aside class="col-sm-4">
						<div class="panel panel-default">
							<h3><a data-toggle="tooltip" title="Search Schedule"> スケジュール <small>(PDF/EXCEL)</small></a></h3>
							<div class="panel-body panel-inner-xs">
								<ul class="list-group file-type">
									<li class="list-group-item"><a href="schedule2.php" title="Export Schedule PDF">輸出  スケジュール</a><a href="schedule2.php" class="pull-right btn btn-danger btn-xs"><i class="fa fa-file-pdf-o"></i></a></li>
									<li class="list-group-item"><a href="schedule3.php" title="Export Schedule Excel">輸出  スケジュール</a><a href="schedule3.php" class="pull-right btn btn-success btn-xs"><i class="fa fa-file-excel-o"></i></a></li>
									<li class="list-group-item"><a href="schedule4.php" title="Import Schedule PDF">スケジュール</a><a href="schedule4.php" class="pull-right btn btn-danger btn-xs"><i class="fa fa-file-pdf-o"></i></a></li>
									<li class="list-group-item"><a href="schedule4.php" title="Import Schedule Excel">スケジュール</a><a href="schedule4.php" class="pull-right btn btn-success btn-xs"><i class="fa fa-file-excel-o"></i></a></li>
								</ul>
							</div> 
						</div>
					</aside>