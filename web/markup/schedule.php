<?php include "inc.header.php";?>
			<section class="container main">
				<div class="row">
					<div class="col-sm-12 inner-side-md maincontent">
						<!-- <h4>
							The World's NVOCC Market Leader ! 200+ Offices in over 90 countries.
						</h4> -->
						<h1>
							エクスポートスケジュール
						</h1>
						<hr>
						<p>各航路の月間スケジュールを確認することができます。（スケジュールの変更が有り得ますので、ご理解を賜りますようお願い申し上げます。）</p>
						<div class="row">
							<div class="row">
								<div class="col-md-11 col-offset-1">
									<div class="well login">
										<h4><a data-toggle="tooltip" title="Search Schedule">スケジュール サーチ</a></h4>
										<form class="form-inline">
										 	<div class="form-group">
										    	<label for="inputEmail3" class="control-label">ポート起源:</label>
											    <select class="form-control">
													  <option>Nagoya</option>
													  <option>Tokyo</option>
													</select>
											</div>
										  	<div class="form-group">
											  	<div class="col-sm-12">
												    <label for="inputPassword3" class="control-label">ポート宛先:</label>
												   	<select class="form-control">
													  <option>Philippines</option>
													  <option>United States of America</option>
												
													</select>
												    <select class="form-control">
													  <option>Manila</option>
													  <option>Cebu</option>
													  <option>Davao</option>
													</select>
												</div>  
										  	</div>
										  	<div class="form-group">
											   	<button type="submit" class="btn btn-primary">サーチ</button>
											</div>
										</form>
									</div>
								</div>
							</div>
							<!-- <div class="row">
								<div class="col-sm-8">
									<p>Select appropriate area</p>
									<div class="col-sm-12 outer-side-sm">
										<ul class="nav nav-pills">
											<li><a href="schedule-asia.php" class="btn btn-primary" title="Asia">Asia</a></li>
											<li><a href="schedule-china.php" class="btn btn-primary" title="China">China</a></li>
											<li><a href="schedule-europe.php" class="btn btn-primary" title="Europe">Europe</a></li>
											<li><a href="schedule-usa.php" class="btn btn-primary" title="USA">USA</a></li>
											<li><a href="schedule-asia.php" class="btn btn-primary" title="Mediterranean">Mediterranean</a></li>
											<li><a href="schedule-asia.php" class="btn btn-primary" title="Canada">Canada</a></li>
											<li><a href="schedule-asia.php" class="btn btn-primary" title="Latin America">Latin America</a></li>
											<li><a href="schedule-asia.php" class="btn btn-primary" title="Africa">Africa</a></li>
											<li><a href="schedule-asia.php" class="btn btn-primary" title="Middle East">Middle East</a></li>
											<li><a href="schedule-asia.php" class="btn btn-primary" title="Oceania">Oceania</a></li>
										</ul>
									</div>
									<br><br>
									<p>Dangerous Goods / Products</p>
									<div class="col-sm-12 outer-side-sm">
										<ul class="nav nav-pills">
											<li><a href="schedule-asia.php" class="btn btn-warning">Asia</a></li>
											<li><a href="schedule-asia.php" class="btn btn-warning">China</a></li>
											<li><a href="schedule-asia.php" class="btn btn-warning">USA</a></li>
											<li><a href="schedule-asia.php" class="btn btn-warning">Europe and MED</a></li>
											<li><a href="schedule-asia.php" class="btn btn-warning">Middle East</a></li>
										</ul>
									</div>
								</div>
								<?php //include "inc.sidebar2.php";?>
							</div> -->
							
							</div>
							
						</div>
						
						<hr>
						<div class="col-sm-12 table-schedule">
							<div class="row">
								<h2>スケジュール一覧</h2>
								<ul>
									<li>
										<div class="media">
											<div class="pull-left">
			  									<a href="#" class="pdf">
													<i class="fa fa-file-pdf-o"></i>
												</a>
												<a href="#" class="excel">
													<i class="fa fa-file-excel-o"></i>
												</a>
											</div>
		  									<div class="media-body">
												<h4 class="media-heading">Schedule Title</h4>
											    <p>Port Origin: Tokyo - Port Destination: Manila</p>
											</div>
										</div>
									</li>
									<li>
										<div class="media">
											<div class="pull-left">
			  									<a href="#" class="pdf">
													<i class="fa fa-file-pdf-o"></i>
												</a>
												<a href="#" class="excel">
													<i class="fa fa-file-excel-o"></i>
												</a>
											</div>
		  									<div class="media-body">
												<h4 class="media-heading">Schedule Title</h4>
											    <p>Port Origin: Tokyo - Port Destination: Manila</p>
											</div>
										</div>
									</li>
								</ul>

								<!-- <div class="pull-right"><a href="#" class="btn"><i class="icon-pdf pull-left"></i></a> ここでは、<a href="#">Adobe Acrobat</a>ファイルをクリックするために</div>
								<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover listing">
                  			<thead>
                      			<tr role="row">
                      				<th>PORT</th>
                      				<th><a title="Tokyo">東京</title></a></th>
                      				<th><a title="Yokohama">横浜</a></th>
                      				<th><a title="Nagoya">名古屋</a></th>
                      				<th><a title="Osaka">大阪</a></th>
                      				<th><a title="Kobe">神戸</a></th>
                      				<th><a title="Moji">マイ</a></th>
                      				<th><a title="Hakata">紹介</a></th>
                      				<th><a title="Hiroshima">広島</a></th>
                      				<th><a title="Shimizu">清水</a></th>
                      				<th><a title="Ishikari">石狩</a></th>
                      				<th><a title="Tomakomai">苫小牧</a></th>
                      				<th><a title="Sendai">仙台</a></th>
                      				<th><a title="Nigata">新潟</a></th>
                      				<th><a title="Toyama">富山</a></th>
                      				<th><a title="Kanazawa">金沢</a></th>
                      				<th><a title="Naha">那覇市</a></th>
                      			</tr>
                  			</thead>   
                  			<tbody>
                  				<tr>
                   					<td>Bangkok</td>
		                            <td>
                            			<a class="btn" href="#">
                                			<i class="icon-excel"></i>
                                		</a>
	                   				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                   				</tr>
                   				<tr>
                   					<td>Laem Chabang</td>
		                            <td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                   				</tr>
                   				<tr>
                   					<td>Lat Krabang</td>
		                            <td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                   				</tr>
                   			
                   				<tr>
                   					<td>Chittagong</td>
		                            <td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                   				</tr>
                   			
                   				<tr>
                   					<td>Muara</td>
		                            <td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                   				</tr>
                   				
                   				<tr>
                   					<td>Phnom Penh</td>
		                            <td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                   				</tr>
                   				<tr>
                   					<td>Sihanoukville</td>
		                            <td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                   				</tr>
                   				<tr>
                   					<td>Bangkok</td>
		                            <td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                   				</tr>
                   				
                   				<tr>
                   					<td>Hongkong</td>
		                            <td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                   				</tr>
                   				
                   				<tr>
                   					<td>Jakarta</td>
		                            <td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                   				</tr>
                   				<tr>
                   					<td>Semarang</td>
		                            <td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                   				</tr>
                   				<tr>
                   					<td>Surabaya</td>
		                            <td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                   				</tr>
                   				
                   				<tr>
                   					<td>Penang</td>
		                            <td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                   				</tr>
                   				
                   				<tr>
                   					<td>Yangon</td>
		                            <td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                   				</tr>
                   				
                   				<tr>
                   					<td>Karachi</td>
		                            <td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                   				</tr>
                   				
                   				<tr>
                   					<td>Cebu</td>
		                            <td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                   				</tr>
                   				<tr>
                   					<td>Davao</td>
		                            <td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                   				</tr>
                   				<tr>
                   					<td>Manila-North</td>
		                            <td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                   				</tr>
                   				<tr>
                   					<td>Manila-South</td>
		                            <td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                       				<td>
                            			
		                                <a class="btn" href="#">
		                                    <i class="icon-excel"></i>
		                                </a>
                       				</td>
                   				</tr>
                    		</tbody>
		                		</table> -->
		                	</div>
		                </div>
					</div>
					<?php //include "inc.sidebar.php";?>
				</div>
			</section>
		</main>
<?php include "inc.footer.php";?>
		