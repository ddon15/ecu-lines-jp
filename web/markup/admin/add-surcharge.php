<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Meta -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Eculine</title>
		<!-- Bootstrap Core CSS -->
		<link href="../assets/css/eculine.css" rel="stylesheet">
		<!-- HTML5 elements and media queries Support for IE8 : HTML5 shim and Respond.js -->
		<!--[if lt IE 9]>
		<script src="../assets/js/html5shiv.js"></script>
		<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
		<!-- Favicon -->
		<link rel="apple-touch-icon-precomposed" sizes="57x57" href="../assets/images/apple-touch-icon-57x57.png" />
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114x114.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72x72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144x144.png" />
		<link rel="apple-touch-icon-precomposed" sizes="60x60" href="../assets/images/apple-touch-icon-60x60.png" />
		<link rel="apple-touch-icon-precomposed" sizes="120x120" href="../assets/images/apple-touch-icon-120x120.png" />
		<link rel="apple-touch-icon-precomposed" sizes="76x76" href="../assets/images/apple-touch-icon-76x76.png" />
		<link rel="apple-touch-icon-precomposed" sizes="152x152" href="../assets/images/apple-touch-icon-152x152.png" />
		<link rel="icon" type="image/png" href="../assets/images/favicon-196x196.png" sizes="196x196" />
		<link rel="icon" type="image/png" href="../assets/images/favicon-96x96.png" sizes="96x96" />
		<link rel="icon" type="image/png" href="../assets/images/favicon-32x32.png" sizes="32x32" />
		<link rel="icon" type="image/png" href="../assets/images/favicon-16x16.png" sizes="16x16" />
		<link rel="icon" type="image/png" href="../assets/images/favicon-128.png" sizes="128x128" />
		<meta name="application-name" content="&nbsp;"/>
		<meta name="msapplication-TileColor" content="#FFFFFF" />
		<meta name="msapplication-TileImage" content="../assets/images/mstile-144x144.png" />
		<meta name="msapplication-square70x70logo" content="../assets/images/mstile-70x70.png" />
		<meta name="msapplication-square150x150logo" content="../assets/images/mstile-150x150.png" />
		<meta name="msapplication-wide310x150logo" content="../assets/images/mstile-310x150.png" />
		<meta name="msapplication-square310x310logo" content="../assets/images/mstile-310x310.png" />
	</head>
	<body>
		<header id="header">
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<a href="dashboard-main.html"><img src="../assets/images/logo.png" class="logo" alt="" title="Ecu-line"></a>
					</div>
					<div class="pull-right">
						<div class="navbar">
							<ul class="nav navbar-nav menu-top">
								<li><a href="#"><i class="fa fa-envelope"></i>Logs<span class="badge"> 10 </span></a></li>
								<li><a href="#"><i class="fa fa-flag"></i>Notifications<span class="badge"> 10 </span></a></li>
								<li><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-user"></i>Firstname Lastname<i class="fa fa-caret-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="manage-profile.php"><i class="icon-edit"></i> Manage Account</a></li>
									<li class="divider"></li>
									<li class="text">Logged in as:</li>
									<li class="text">email@domain.com</li>
									<li class="divider"></li>
									<li><a href="#"><i class="fa fa-sign-out"></i> Logout</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<ul class="nav nav-tabs nav-menu admin-menu">
				<li><a href="dashboard.php" title="Dashboard">Dashboard</a></li>
				<li><a href="schedule-listing.php" title="Schedule">スケジュール</a></li>
				<li><a href="surcharge-listing.php" title="Surcharge">サーチャージ</a></li>
				<li><a href="service-listing.php" title="Service">Service</a></li>
				<li><a href="user-listing.php" title="User">Users</a></li>
				<li><a href="jobs-listing.php" title="Job Management">Jobs</a></li>
			</ul>
		</div>
	</header>
	<main class="admin">
	<section class="container main">
		<div class="row">
			<div class="col-md-12 inner-right inner-bottom-md">
				<ul class="breadcrumb">
					<li>
						<a href="dashboard.php"><i class="fa fa-home"></i></a>
					</li>
					<li class="active">
						Add Surcharge
					</li>
				</ul>
				<div class="row">
					<div class="col-sm-12 item">
						<div class="col-md-8 col-md-offset-2">
							<h1>
							<i class="fa fa-user fa3"></i> Add New Surcharge
							</h1>
							<hr>
							<form action="#" method="post">
								<section class="section">
									<h2>Surcharge Information</h2>
									<div class="row">
										<div class="well">
											<div class="form-group">
												<label for="surcharge">Surcharge Name</label>
												<input type="text" class="form-control" required="" placeholder="e.g November Export - Cebu to Tokyo">
											</div>
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="account-firstname">Month</label>
													<input type="text" class="form-control" required=""> <!-- -used select for months -->
												</div>
												<div class="form-group col-sm-6">
													<label for="account-firstname">Year</label>
													<input type="text" class="form-control" required="" placeholder="e.g 2014">
												</div>
											</div>
											<div class="row">
												<div class="col-sm-12">
													<textarea class="form-control" rows="3">web editor here</textarea>
												</div>
											</div>
											<div class="clear">&nbsp;</div>
											<div class="row">
												<div class="col-sm-4">
													<div class="form-group">
													    <label for="exampleInputFile">PDF File</label>
													    <input type="file" id="exampleInputFile">
													    <p class="help-block">Upload PDF File</p>
													</div>
												</div>
												<div class="col-sm-4">
													<div class="form-group">
													    <label for="exampleInputFile">Excel File</label>
													    <input type="file" id="exampleInputFile">
													    <p class="help-block">Upload Excel File</p>
													</div>
												</div>
												<div class="col-sm-4">
													<div class="form-group">
													    <label for="exampleInputFile">Word File</label>
													    <input type="file" id="exampleInputFile">
													    <p class="help-block">Upload Word File</p>
													</div>
												</div>
											</div>
										</div>
										<div class="row terms">
											<div class="col-sm-12">
												<button class="btn pull-right btn-primary btn-lg">Submit</button>
												<button class="btn pull-right btn-lg btn-link">Cancel</button>
											</div>
										</div>	
									</div>		
								</section>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	</main>
	<?php include "inc.footer.php";?>