<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Meta -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Eculine</title>
		<!-- Bootstrap Core CSS -->
		<link href="../../assets/css/eculine.css" rel="stylesheet">
		<!-- HTML5 elements and media queries Support for IE8 : HTML5 shim and Respond.js -->
		<!--[if lt IE 9]>
			<script src="../assets/js/html5shiv.js"></script>
			<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
		<!-- Favicon -->
		<link rel="apple-touch-icon-precomposed" sizes="57x57" href="../assets/images/apple-touch-icon-57x57.png" />
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114x114.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72x72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144x144.png" />
		<link rel="apple-touch-icon-precomposed" sizes="60x60" href="../assets/images/apple-touch-icon-60x60.png" />
		<link rel="apple-touch-icon-precomposed" sizes="120x120" href="../assets/images/apple-touch-icon-120x120.png" />
		<link rel="apple-touch-icon-precomposed" sizes="76x76" href="../assets/images/apple-touch-icon-76x76.png" />
		<link rel="apple-touch-icon-precomposed" sizes="152x152" href="../assets/images/apple-touch-icon-152x152.png" />
		<link rel="icon" type="image/png" href="../assets/images/favicon-196x196.png" sizes="196x196" />
		<link rel="icon" type="image/png" href="../assets/images/favicon-96x96.png" sizes="96x96" />
		<link rel="icon" type="image/png" href="../assets/images/favicon-32x32.png" sizes="32x32" />
		<link rel="icon" type="image/png" href="../assets/images/favicon-16x16.png" sizes="16x16" />
		<link rel="icon" type="image/png" href="../assets/images/favicon-128.png" sizes="128x128" />
		<meta name="application-name" content="&nbsp;"/>
		<meta name="msapplication-TileColor" content="#FFFFFF" />
		<meta name="msapplication-TileImage" content="../assets/images/mstile-144x144.png" />
		<meta name="msapplication-square70x70logo" content="../assets/images/mstile-70x70.png" />
		<meta name="msapplication-square150x150logo" content="../assets/images/mstile-150x150.png" />
		<meta name="msapplication-wide310x150logo" content="../assets/images/mstile-310x150.png" />
		<meta name="msapplication-square310x310logo" content="../assets/images/mstile-310x310.png" />
	</head>
	<body>
		<header class="container">
			<div class="navbar">
				<div class="navbar-header">
						<a class="navbar-brand col-sm-2 inner-side-sm" href="index.php"><img src="../assets/images/logo.png" class="logo" alt="" title="Ecu-line"></a>
						<a class="btn responsive-menu pull-right" data-toggle="collapse" data-target=".navbar-collapse"><i class='icon-menu-1'></i></a>
				</div><!-- /.navbar-header -->
			</div><!-- /.navbar -->
		</header>
		<main>
			<section class="container main">
				<div class="row">
					<div class="col-md-6 col-md-offset-3 inner-right inner-bottom-md">
						<h2>Login</h2>
						<div class="well">
							<form class="forms login">
								<div class="form-group">
									<div class="row">
										<div class="col-sm-12">
											<label for="username">Username</label>
											<input type="text" name="name" class="form-control">
										</div><!-- /.col -->
									</div><!-- /.row -->
									<div class="row">
										<div class="col-sm-12">
										<label for="password">Password</label>
										<input type="password" name="password" class="form-control">
										<span class="pull-right"><a href="#">Forgot Password</a></span>
										</div><!-- /.col -->
									</div><!-- /.row -->
									 <div class="checkbox">
											<label><input type="checkbox"> <span>Remember Me</span></label>
										</div>
									<a href="dashboard.php" class="pull-right btn btn-primary btn-lg btn-submit">Submit</a>
								</div>
							</form>
						<div id="response"></div>
						<div class="clear" style="height:40px;">&nbsp;</div>
						</div>
					</div>
				</div>

			</section>
		</main>
<?php include "inc.footer.php";?>
		