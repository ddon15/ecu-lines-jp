<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Meta -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Eculine</title>
		<!-- Bootstrap Core CSS -->
		<link href="../assets/css/eculine.css" rel="stylesheet">
		<!-- HTML5 elements and media queries Support for IE8 : HTML5 shim and Respond.js -->
		<!--[if lt IE 9]>
		<script src="../assets/js/html5shiv.js"></script>
		<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
		<!-- Favicon -->
		<link rel="apple-touch-icon-precomposed" sizes="57x57" href="../assets/images/apple-touch-icon-57x57.png" />
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114x114.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72x72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144x144.png" />
		<link rel="apple-touch-icon-precomposed" sizes="60x60" href="../assets/images/apple-touch-icon-60x60.png" />
		<link rel="apple-touch-icon-precomposed" sizes="120x120" href="../assets/images/apple-touch-icon-120x120.png" />
		<link rel="apple-touch-icon-precomposed" sizes="76x76" href="../assets/images/apple-touch-icon-76x76.png" />
		<link rel="apple-touch-icon-precomposed" sizes="152x152" href="../assets/images/apple-touch-icon-152x152.png" />
		<link rel="icon" type="image/png" href="../assets/images/favicon-196x196.png" sizes="196x196" />
		<link rel="icon" type="image/png" href="../assets/images/favicon-96x96.png" sizes="96x96" />
		<link rel="icon" type="image/png" href="../assets/images/favicon-32x32.png" sizes="32x32" />
		<link rel="icon" type="image/png" href="../assets/images/favicon-16x16.png" sizes="16x16" />
		<link rel="icon" type="image/png" href="../assets/images/favicon-128.png" sizes="128x128" />
		<meta name="application-name" content="&nbsp;"/>
		<meta name="msapplication-TileColor" content="#FFFFFF" />
		<meta name="msapplication-TileImage" content="../assets/images/mstile-144x144.png" />
		<meta name="msapplication-square70x70logo" content="../assets/images/mstile-70x70.png" />
		<meta name="msapplication-square150x150logo" content="../assets/images/mstile-150x150.png" />
		<meta name="msapplication-wide310x150logo" content="../assets/images/mstile-310x150.png" />
		<meta name="msapplication-square310x310logo" content="../assets/images/mstile-310x310.png" />
	</head>
	<body>
		<header id="header">
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<a href="dashboard-main.html"><img src="../assets/images/logo.png" class="logo" alt="" title="Ecu-line"></a>
					</div>
					<div class="pull-right">
						<div class="navbar">
							<ul class="nav navbar-nav menu-top">
								<li><a href="#"><i class="fa fa-envelope"></i>Logs<span class="badge"> 10 </span></a></li>
								<li><a href="#"><i class="fa fa-flag"></i>Notifications<span class="badge"> 10 </span></a></li>
								<li><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-user"></i>Firstname Lastname<i class="fa fa-caret-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="manage-profile.php"><i class="icon-edit"></i> Manage Account</a></li>
									<li class="divider"></li>
									<li class="text">Logged in as:</li>
									<li class="text">email@domain.com</li>
									<li class="divider"></li>
									<li><a href="#"><i class="fa fa-sign-out"></i> Logout</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<ul class="nav nav-tabs nav-menu admin-menu">
				<li><a href="dashboard.php" title="Dashboard">Dashboard</a></li>
				<li class="active"><a href="schedule-listing.php" title="Schedule">スケジュール</a></li>
				<li><a href="surcharge-listing.php" title="Surcharge">サーチャージ</a></li>
				<li><a href="service-listing.php" title="Service">Service</a></li>
				<li><a href="user-listing.php" title="User">Users</a></li>
				<li><a href="jobs-listing.php" title="Job Management">Jobs</a></li>
			</ul>
		</div>
	</header>
	<main class="admin">
		<section class="container main">
			<div class="row">
				<div class="col-md-12 inner-right inner-bottom-md">
					<ul class="breadcrumb">
						<li>
							<a href="dashboard.php"><i class="fa fa-home"></i></a>
						</li>
						<li class="active">
							Schedule List
						</li>
					</ul>
					<div class="col-sm-12 item">
						<h1><i class="fa fa-calendar fa3"></i>Schedule Listing</h1>
						<hr>
						<div class="row">
							<div class="col-sm-12 item">
								<h4 class="pull-left">&nbsp;</h4>
								<div class="pull-right btn-option">
									<a class="btn btn-sm btn-success" href="add-schedule.php"><i class="fa fa-plus-square"></i>Add New Schedule</a>
					            </div>
								<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
		                      			<thead>
		                          			<tr role="row">
		                          				<th>Date and Time Added</th>
		                          				<th>Port Origin</th>
		                          				<th>Port Desitnation</th>
		                          				<th>Month and Year</th>
		                          				<th>File Type</th>
		                          				<th>Actions</th>
		                          			</tr>
		                      			</thead>   
		                      			<tbody>
		                      				<tr>
		                      					<td>10-09-2015 01:24 PM</td>
					                            <td>
					                            	<h4>Osaka</h4>
					                            </td>
					                            <td>
					                                <h4>Cebu</h4>
					                            </td>
					                            <td>November <span>2014</span></td>
					                            <td>
					                            	<a class="btn btn-success" href="#"><i class="fa fa-file-excel-o"></i>Excel</a>
					                            </td>
		                            			<td>
		                                			<a class="btn btn-info" href="#"><i class="fa fa-search-plus"></i>View Details</a>
					               				</td>
		                       				</tr>
		                       				<tr>
		                      					<td>10-09-2015 01:24 PM</td>
					                            <td>
					                            	<h4>Osaka</h4>
					                            </td>
					                            <td>
					                                <h4>Cebu</h4>
					                            </td>
					                            <td>November <span>2014</span></td>
					                            <td>
					                            	<a class="btn btn-danger" href="#"><i class="fa fa-file-pdf-o"></i>Pdf</a>
					                            </td>
		                            			<td>
		                                			<a class="btn btn-info" href="#"><i class="fa fa-search-plus"></i>View Details </a>
					               				</td>
		                       				</tr>
		                       						<tr>
		                      					<td>10-09-2015 01:24 PM</td>
					                            <td>
					                            	<h4>Osaka</h4>
					                            </td>
					                            <td>
					                                <h4>Cebu</h4>
					                            </td>
					                            <td>November <span>2014</span></td>
					                            <td>
					                            	<a class="btn btn-success" href="#"><i class="fa fa-file-excel-o"></i>Excel</a>
					                            </td>
		                            			<td>
		                                			<a class="btn btn-info" href="#"><i class="fa fa-search-plus"></i>View Details</a>
					               				</td>
		                       				</tr>
		                       				<tr>
		                      					<td>10-09-2015 01:24 PM</td>
					                            <td>
					                            	<h4>Osaka</h4>
					                            </td>
					                            <td>
					                                <h4>Cebu</h4>
					                            </td>
					                            <td>November <span>2014</span></td>
					                            <td>
					                            	<a class="btn btn-danger" href="#"><i class="fa fa-file-pdf-o"></i>Pdf</a>
					                            </td>
		                            			<td>
		                                			<a class="btn btn-info" href="#"><i class="fa fa-search-plus"></i>View Details </a>
					               				</td>
		                       				</tr>
		                       						<tr>
		                      					<td>10-09-2015 01:24 PM</td>
					                            <td>
					                            	<h4>Osaka</h4>
					                            </td>
					                            <td>
					                                <h4>Cebu</h4>
					                            </td>
					                            <td>November <span>2014</span></td>
					                            <td>
					                            	<a class="btn btn-success" href="#"><i class="fa fa-file-excel-o"></i>Excel</a>
					                            </td>
		                            			<td>
		                                			<a class="btn btn-info" href="#"><i class="fa fa-search-plus"></i>View Details</a>
					               				</td>
		                       				</tr>
		                       				<tr>
		                      					<td>10-09-2015 01:24 PM</td>
					                            <td>
					                            	<h4>Osaka</h4>
					                            </td>
					                            <td>
					                                <h4>Cebu</h4>
					                            </td>
					                            <td>November <span>2014</span></td>
					                            <td>
					                            	<a class="btn btn-danger" href="#"><i class="fa fa-file-pdf-o"></i>Pdf</a>
					                            </td>
		                            			<td>
		                                			<a class="btn btn-info" href="#"><i class="fa fa-search-plus"></i>View Details </a>
					               				</td>
		                       				</tr>
		                       						<tr>
		                      					<td>10-09-2015 01:24 PM</td>
					                            <td>
					                            	<h4>Osaka</h4>
					                            </td>
					                            <td>
					                                <h4>Cebu</h4>
					                            </td>
					                            <td>November <span>2014</span></td>
					                            <td>
					                            	<a class="btn btn-success" href="#"><i class="fa fa-file-excel-o"></i>Excel</a>
					                            </td>
		                            			<td>
		                                			<a class="btn btn-info" href="#"><i class="fa fa-search-plus"></i>View Details</a>
					               				</td>
		                       				</tr>
		                       				<tr>
		                      					<td>10-09-2015 01:24 PM</td>
					                            <td>
					                            	<h4>Osaka</h4>
					                            </td>
					                            <td>
					                                <h4>Cebu</h4>
					                            </td>
					                            <td>November <span>2014</span></td>
					                            <td>
					                            	<a class="btn btn-danger" href="#"><i class="fa fa-file-pdf-o"></i>Pdf</a>
					                            </td>
		                            			<td>
		                                			<a class="btn btn-info" href="#"><i class="fa fa-search-plus"></i>View Details </a>
					               				</td>
		                       				</tr>
		                       						<tr>
		                      					<td>10-09-2015 01:24 PM</td>
					                            <td>
					                            	<h4>Osaka</h4>
					                            </td>
					                            <td>
					                                <h4>Cebu</h4>
					                            </td>
					                            <td>November <span>2014</span></td>
					                            <td>
					                            	<a class="btn btn-success" href="#"><i class="fa fa-file-excel-o"></i>Excel</a>
					                            </td>
		                            			<td>
		                                			<a class="btn btn-info" href="#"><i class="fa fa-search-plus"></i>View Details</a>
					               				</td>
		                       				</tr>
		                       				<tr>
		                      					<td>10-09-2015 01:24 PM</td>
					                            <td>
					                            	<h4>Osaka</h4>
					                            </td>
					                            <td>
					                                <h4>Cebu</h4>
					                            </td>
					                            <td>November <span>2014</span></td>
					                            <td>
					                            	<a class="btn btn-danger" href="#"><i class="fa fa-file-pdf-o"></i>Pdf</a>
					                            </td>
		                            			<td>
		                                			<a class="btn btn-info" href="#"><i class="fa fa-search-plus"></i>View Details </a>
					               				</td>
		                       				</tr>

		                        		</tbody>
		                        </table>
		                        <!-- this will only show if more than 10 listing -->
								<nav>
									<ul class="pagination">
									    <li>
									      <a href="#" aria-label="Previous">
									        <span aria-hidden="true">&laquo;</span>
									      </a>
									    </li>
									    <li><a href="#">1</a></li>
									    <li><a href="#">2</a></li>
									    <li><a href="#">3</a></li>
									    <li><a href="#">4</a></li>
									    <li><a href="#">5</a></li>
									    <li>
									      <a href="#" aria-label="Next">
									        <span aria-hidden="true">&raquo;</span>
									      </a>
									    </li>
								  	</ul>
								</nav>
	                        </div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>
	<?php include "inc.footer.php";?>