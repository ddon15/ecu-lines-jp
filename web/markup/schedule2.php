<?php include "inc.header.php";?>
			<section class="container main">
				<div class="row">
					<div class="col-sm-8 inner-side-md maincontent">
						<!-- <h4>
							The World's NVOCC Market Leader ! 200+ Offices in over 90 countries.
						</h4> -->
						<h1>
							スケジュール
						</h1>
						<hr>
						<div class="row">
								<div class="col-sm-8 col-sm-offset-2">
							<div class="well login">
								<h4>検索スケジュール</h4>
								<form class="form-horizontal">
									<div class="form-group">
									    <div class="col-sm-12">
											<input type="text" class="form-control" placeholder="e.g Tokyo">
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-12">
											<button type="submit" class="btn btn-primary">提出します</button>
										</div>
									</div>
								</form>
							</div>
						</div>
						</div>

						<p>Select appropriate area</p>
						<div class="col-sm-12 outer-side-sm">
							<ul class="nav nav-pills">
								<li><a href="schedule-asia.php" class="btn btn-primary" title="Asia">Asia</a></li>
								<li><a href="schedule-china.php" class="btn btn-primary" title="China">China</a></li>
								<li><a href="schedule-europe.php" class="btn btn-primary" title="Europe">Europe</a></li>
								<li><a href="schedule-usa.php" class="btn btn-primary" title="USA">USA</a></li>
								<li><a href="schedule-asia.php" class="btn btn-primary" title="Mediterranean">Mediterranean</a></li>
								<li><a href="schedule-asia.php" class="btn btn-primary" title="Canada">Canada</a></li>
								<li><a href="schedule-asia.php" class="btn btn-primary" title="Latin America">Latin America</a></li>
								<li><a href="schedule-asia.php" class="btn btn-primary" title="Africa">Africa</a></li>
								<li><a href="schedule-asia.php" class="btn btn-primary" title="Middle East">Middle East</a></li>
								<li><a href="schedule-asia.php" class="btn btn-primary" title="Oceania">Oceania</a></li>
							</ul>
						</div>
						<br><br>
						<p>For Dangerous Goods / Products</p>
						<div class="col-sm-12 outer-side-sm">
							<ul class="nav nav-pills">
								<li><a href="schedule-asia.php" class="btn btn-warning">Asia</a></li>
								<li><a href="schedule-asia.php" class="btn btn-warning">China</a></li>
								<li><a href="schedule-asia.php" class="btn btn-warning">USA</a></li>
								<li><a href="schedule-asia.php" class="btn btn-warning">Europe and MED</a></li>
								<li><a href="schedule-asia.php" class="btn btn-warning">Middle East</a></li>
							</ul>
						</div>
					</div>
					<?php include "inc.sidebar.php";?>
				</div>
			</section>
		</main>
<?php include "inc.footer.php";?>
		