<?php include "inc.header2.php";?>
			<!-- <section id="maincontent" class="content-body download">
				<div class="container">
					<div class="row">
						<div class="col-sm-11 center-block text-center">
							<h1>Downloadable Materials</h1>
							<p>Here in Behavior World, we are always on the lookout for free teaching resources.</p>
						</div>
						<div class="col-sm-11 center-block inner-top-xs">
							<div class="col-sm-3">
								<div class="item">
									<a href="assets/resources/token/bw.pdf" target="_blank">
										<img src="assets/images/buddies/coin/bw-icon.png" alt="">
									</a>
									<label for="Behavior World">Behavior World Token</label>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="item">
									<a href="assets/resources/token/alien.pdf" target="_blank">
										<img src="assets/images/buddies/coin/alien.png" alt="">
									</a>
									<label for="Alien">Alien Token</label>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="item">
									<a href="assets/resources/token/mushrooom.pdf" target="_blank">
										<img src="assets/images/buddies/coin/mushroom.png" alt="">
									</a>
									<label for="Green">Mushroom Token</label>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="item">
									<a href="assets/resources/token/knight.pdf" target="_blank">
										<img src="assets/images/buddies/coin/knight.png" alt="">
									</a>
									<label for="Mushroom">Knight Token</label>
								</div>
							</div>		
						</div>
						<div class="col-sm-11 center-block inner-top-xs">
							<div class="col-sm-3">
								<div class="item">
									<a href="assets/resources/token/green.pdf" target="_blank">
										<img src="assets/images/buddies/coin/green.png" alt="">
									</a>
									<label for="Knight">Green Buddy Token</label>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="item">
									<a href="assets/resources/token/shark.pdf" target="_blank">
										<img src="assets/images/buddies/coin/shark.png" alt="">
									</a>
									<label for="Alien">Shark Token</label>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="item">
									<a href="assets/resources/token/angler-fish.pdf" target="_blank">
										<img src="assets/images/buddies/coin/angler-fish.png" alt="">
									</a>
									<label for="Alien">Angler Fish Token</label>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="item">
									<a href="assets/resources/token/owl.pdf" target="_blank">
										<img src="assets/images/buddies/coin/owl.png" alt="">
									</a>
									<label for="Alien">Owl Token</label>
								</div>
							</div>		
						</div>
						<div class="col-sm-11 center-block inner-top-xs">
							<div class="col-sm-3">
								<div class="item">
									<a href="assets/resources/buddies-superhero-boy.pdf" target="_blank">
										<img src="assets/images/buddies/coin/superheroboy.png" alt="">
									</a>
									<label for="Knight">Super Hero Boy Token</label>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="item">
									<a href="assets/resources/token/superhero-girl.pdf" target="_blank">
										<img src="assets/images/buddies/coin/superherogirl.png" alt="">
									</a>
									<label for="Alien">Super Hero Girl Token</label>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="item">
									<a href="assets/resources/token/wizard.pdf" target="_blank">
										<img src="assets/images/buddies/coin/wizardboy.png" alt="">
									</a>
									<label for="Alien">Wizard Token</label>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="item">
									<a href="assets/resources/token/owl.pdf" target="_blank">
										<img src="assets/images/buddies/coin/owl.png" alt="">
									</a>
									<label for="Alien">Owl Token</label>
								</div>
							</div>		
						</div>
					</div><!-- /.row
				</div><!-- /.container
			</section> -->
			<section id="isotope">
				<div class="container inner">
					<div class="row">
						<div class="col-md-8 col-sm-9 center-block text-center">
							<header>
								<h1>Resources</h1>
								<p>
									We have good news for you! Here are free resources to download for teachers, parents and other individuals working with children.
								</p>
							</header>
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.container -->
				
				<div class="container inner-bottom">
					<div class="row">
						<div class="col-sm-12 portfolio resources">
							
							<ul class="filter text-center">
								<li><a href="#" data-filter="*" class="active">All</a></li>
								<li><a href="#" data-filter=".token">Token</a></li>
								<li><a href="#" data-filter=".coloring">Coloring Stuff</a></li>
								<!-- <li><a href="#" data-filter=".token">Print</a></li>
								<li><a href="#" data-filter=".token">Photography</a></li> -->
							</ul><!-- /.filter -->
							
							<ul class="items col-4">
								
								<li class="item thumb token">
									<a href="assets/resources/token/behaviorworld.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Behavior World Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/coin/bw-icon.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
								<li class="item thumb coloring">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/colored/dragon.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
								
								<li class="item thumb token">
									<a href="assets/resources/token/knight.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Knight Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/coin/knight.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
										<li class="item thumb coloring">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/colored/shark.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
								<li class="item thumb token">
									<a href="assets/resources/token/wizard.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Wizard Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/coin/wizardboy.png" alt="">
									</a>
								</li><!-- /.item -->
								
								<li class="item thumb token">
									<a href="assets/resources/token/mushroom.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Mushrrom Budd Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/coin/mushroom.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
									<li class="item thumb coloring">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/colored/owl.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
								
								<li class="item thumb token">
									<a href="assets/resources/token/owl.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Owl Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/coin/owl.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
								
								<li class="item thumb token">
									<a href="assets/resources/token/alien.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Alien Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/coin/alien.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
								
								
								<li class="item thumb coloring">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/colored/space-girl.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
								<li class="item thumb token">
									<a href="assets/resources/token/angler-fish.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Angler Fish Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/coin/angler-fish.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
								<li class="item thumb token">
									<a href="assets/resources/token/bw-boy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Superhero Boy Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/coin/bw-boy.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
								
							
								<li class="item thumb coloring">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/colored/fairy.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
								<li class="item thumb token">
									<a href="assets/resources/token/bw-girl.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Superhero Girl Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/coin/bw-girl.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
								
								<li class="item thumb token">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/coin/green.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
								<li class="item thumb coloring">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/colored/mushroom.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
								<li class="item thumb coloring">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/colored/alien.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
								<li class="item thumb coloring">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/colored/angler-fish.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
								<li class="item thumb coloring">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/colored/blue-alien.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
								<li class="item thumb token">
									<a href="assets/resources/token/sharkie.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Sharkie Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/coin/shark.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
								<li class="item thumb coloring">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/colored/bw-boy.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
									<li class="item thumb token">
									<a href="assets/resources/token/dragon.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Dragon Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/coin/dragon.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->

								
								<li class="item thumb coloring">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/colored/greenbuddy.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
								<li class="item thumb coloring">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/colored/knight.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
								<li class="item thumb coloring">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/colored/mage.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
								

							
								<li class="item thumb coloring">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/colored/pirate-shark.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
								<li class="item thumb coloring">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/colored/princess.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
						
								<li class="item thumb coloring">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info -->
											</figcaption>
											<img src="assets/images/buddies/colored/space-boy.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
								
								<!-- <li class="item thumb coloring">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info 
											</figcaption>
											<img src="assets/images/buddies/coin/green.png" alt="">
										</figure>
									</a>
								</li><!-- /.item -->
								<!-- <li class="item thumb token">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info 
											</figcaption>
											<img src="assets/images/buddies/coin/green.png" alt="">
										</figure>
									</a>
								</li><!-- /.item 
								<li class="item thumb token">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info 
											</figcaption>
											<img src="assets/images/buddies/coin/green.png" alt="">
										</figure>
									</a>
								</li><!-- /.item 
								<li class="item thumb token">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info 
											</figcaption>
											<img src="assets/images/buddies/coin/green.png" alt="">
										</figure>
									</a>
								</li><!-- /.item 
								<li class="item thumb token">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info 
											</figcaption>
											<img src="assets/images/buddies/coin/green.png" alt="">
										</figure>
									</a>
								</li><!-- /.item 
								<li class="item thumb token">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info 
											</figcaption>
											<img src="assets/images/buddies/coin/green.png" alt="">
										</figure>
									</a>
								</li><!-- /.item 
								<li class="item thumb token">
									<a href="assets/resources/greenbuddy.pdf" target="_blank">
										<figure>
											<figcaption class="text-overlay">
												<div class="info">
													<h4>Green Buddy Coin</h4>
													<p>Token</p>
												</div><!-- /.info 
											</figcaption>
											<img src="assets/images/buddies/coin/green.png" alt="">
										</figure>
									</a>
								</li><!-- /.item --> 
								
							</ul><!-- /.items -->
							
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.container -->
			</section>
		</main>
<?php include "inc.footer.php";?>
		