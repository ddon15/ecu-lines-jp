<?php include "inc.header.php";?>
			<section class="container main">
				<div class="row">
					<div class="col-sm-8 inner-side-md maincontent">
						<!-- <h4>
							The World's NVOCC Market Leader ! 200+ Offices in over 90 countries.
						</h4> -->
						<h1>
							User Account Login
						</h1>
						<hr>
						<div class="col-sm-8 col-sm-offset-2">
						<div class="well login">
							<form class="form-horizontal">
								<div class="form-group">
									<label for="username" class="col-sm-3 control-label">Username:</label>
								    <div class="col-sm-9">
										<input type="text" class="form-control" placeholder="your username">
									</div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Password:</label>
									<div class="col-sm-9">
										<input type="password" class="form-control">
									</div>
								</div>
								<p class="pull-right"><a href="#">Forgot Password?</a></p>
								<div class="checkbox">
								    <label>
								      <input type="checkbox"> Remember Login
								    </label>
							  	</div>
							  	<br>
								<div class="form-group">
									<div class="col-sm-12">
										<button type="submit" class="btn btn-primary">提出します</button>
									</div>
								</div>

							</form>
						</div>
						</div>
					</div>
					<?php include "inc.sidebar.php";?>
				</div>
			</section>
		</main>
<?php include "inc.footer.php";?>
		