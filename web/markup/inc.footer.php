		<footer>
			<div class="container">
				<div class="row">
					<ul class="nav navbar-nav">
						<li><a href="index.php" title="Home">ホーム</a></li>
						<li>|</li>
						<li><a href="http://theavvashyagroup.com/" target="_blank">The Avvashya Group</a></li>
						<li>|</li>
						<li><a href="http://theavvashyagroup.com/corporate-social-responsibility-csr/" target="_blank">CSR Initiatives</a></li>
						<li>|</li>
						<li><a href="terms.php">Terms Of Use</a></li>
						<li>|</li>
						<li><a href="privacy-policy.php">Privacy Statement</a></li>
						<li>|</li>
						<li><a href="http://webapplications.as.eculine.net/ecustatisticsjp/index.aspx" target="_blank">Sign in</a></li>
					</ul>
				</div><!-- /.row --> 
				<p>Copyright © ECULine - Japan, Ltd. All rights reserved.</p>
			</div><!-- .container -->
		</footer>
		<script src="../assets/js/jquery.min.js"></script>
		<script src="../assets/js/jquery.easing.1.3.min.js"></script>
		<script src="../assets/js/jquery.form.js"></script>
		<script src="../assets/js/jquery.validate.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
	</body>
</html>