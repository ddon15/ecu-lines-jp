<?php include "inc.header.php";?>
			<section class="container main">
				<div class="row">
					<div class="col-sm-8 inner-side-md maincontent">
						<!-- <h4>
							The World's NVOCC Market Leader ! 200+ Offices in over 90 countries.
						</h4> -->
						<h1>
							スケジュール
						</h1>
						<hr>
						<p>Select appropriate area</p>
						<div class="row outer-side-sm">
							<div class="col-md-3"><a href="schedule5.php" class="btn btn-success">Asia</a></div>
							<div class="col-md-3"><a href="schedule5.php" class="btn btn-success">Middle East</a></div>
							<div class="col-md-3"><a href="schedule5.php" class="btn btn-success">Europe</a></div>
							<div class="col-md-3"><a href="schedule5.php" class="btn btn-success">Mediterranean</a></div>
						</div>
						<div class="row outer-side-sm">
							
							<div class="col-md-3"><a href="schedule5.php" class="btn btn-success">Canada</a></div>
							<div class="col-md-3"><a href="schedule5.php" class="btn btn-success">America</a></div>
							<div class="col-md-3"><a href="schedule5.php" class="btn btn-success">Latin America</a></div>
							<div class="col-md-3"><a href="schedule5.php" class="btn btn-success">Africa</a></div>
						</div>
						<div class="row outer-side-sm">
							<div class="col-md-3"><a href="schedule5.php" class="btn btn-success">Chinese</a></div>
							<div class="col-md-3"><a href="schedule5.php" class="btn btn-success">Oceania</a></div>
							<div class="col-md-3"></div>
							<div class="col-md-3"></div>
						</div>
						<br><br>
						<p>For Dangerous Goods / Products</p>
						<div class="row outer-side-sm">
							<div class="col-md-3"><a href="schedule5.php" class="btn btn-danger">Asia</a></div>
							<div class="col-md-3"><a href="schedule5.php" class="btn btn-danger">Middle East</a></div>
							<div class="col-md-3"><a href="schedule5.php" class="btn btn-danger">United States</a></div>
							<div class="col-md-3"><a href="schedule5.php" class="btn btn-danger">China</a></div>
						</div>
						<div class="row outer-side-sm">
							
							<div class="col-md-4"><a href="schedule5.php" class="btn btn-danger">Europe and the Mediterranean</a></div>
							<div class="col-md-4"></div>
						</div>
					</div>
					<?php include "inc.sidebar.php";?>
				</div>
			</section>
		</main>
<?php include "inc.footer.php";?>
		