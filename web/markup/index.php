<?php include "inc.header.php";?>
<section class="container">
	<div class="row">
		<div class="col-sm-8 inner-side-md maincontent">
			<!-- <h4>
				The World's NVOCC Market Leader ! 200+ Offices in over 90 countries.
			</h4> -->
			<h4>
			The World's NVOCC Market Leader ! 200+ Offices in over 90 countries.
			</h4>
			<!-- <hr> -->
			<!-- <img src="../assets/images/HomeBanner.jpg" alt="Eculine Hero Image" class="hero-image"> -->
			<div class="row">
				<div class="col-md-12 inner-side-md">
					<div class="panel panel-default">
						<!-- <h3><a data-toggle="tooltip" title="Pick Up"> ピックアップ</a></h3> -->
						<div class="panel-body">
							<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
								<!-- Indicators -->
								<ol class="carousel-indicators">
									<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
									<li data-target="#carousel-example-generic" data-slide-to="1"></li>
									<li data-target="#carousel-example-generic" data-slide-to="2"></li>
								</ol>
								<!-- Wrapper for slides -->
								<div class="carousel-inner">
									<div class="item active">
										<img src="../assets/images/homebanner2.jpg" alt="...">
										<div class="carousel-caption">
											<h4>Caption Text</h4>
										</div>
									</div>
									<div class="item">
										<img src="../assets/images/homebanner.jpg" alt="...">
										<div class="carousel-caption">
											<h4>Caption Text 2</h4>
										</div>
									</div>
									<div class="item">
										<img src="../assets/images/homebanner3.jpg" alt="...">
										<div class="carousel-caption">
											<h4>Caption Text Here </h4>
										</div>
									</div>
								</div>
								
								<!-- Controls -->
								<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								</a>
								<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								</a>
								</div> <!-- Carousel -->
							</div>
						</div>
					</div>
				</div>
				<!-- <div class="row">
						<div class="col-md-4 inner-side-md">
								<div class="panel panel-default">
										<h3><a data-toggle="tooltip" title="Search Schedule"> お見積り</a></h3>
										<div class="panel-body panel-inner-sm">
												<ul class="list-group">
														<li class="list-group-item"><a href="#">輸出  スケジュール</a></li>
														<li class="list-group-item"><a href="#">輸入 スケジュール</a></li>
														<li class="list-group-item"><a href="#">スケジュール</a></li>
												</ul>
										</div>
								</div>
						</div>
						<div class="col-md-8 inner-side-md">
								<div class="panel panel-default">
										<h3><a data-toggle="tooltip" title="Search Schedule"> お見積り</a></h3>
										<div class="panel-body panel-inner-sm">
												<ul class="list-group">
														<li class="list-group-item"><a href="#">輸出  スケジュール</a></li>
														<li class="list-group-item"><a href="#">輸入 スケジュール</a></li>
														<li class="list-group-item"><a href="#">スケジュール</a></li>
												</ul>
										</div>
								</div>
						</div>
				</div> -->
				<div class="row">
					<div class="col-md-4 inner-side-md">
						<div class="panel panel-default">
							<h3><a data-toggle="tooltip" title="Search Schedule"> お見積り</a></h3>
							<div class="panel-body panel-inner-sm">
								<ul class="list-group">
									<li class="list-group-item"><a href="#">輸出  スケジュール</a></li>
									<li class="list-group-item"><a href="#">輸入 スケジュール</a></li>
									<li class="list-group-item"><a href="#">スケジュール</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-4 inner-side-md">
						<div class="panel panel-default">
							<h3><a data-toggle="tooltip" title="Search Schedule"> 各種サービス</a></h3>
							<div class="panel-body panel-inner-sm">
								<ul class="list-group">
									<li class="list-group-item"><a href="#">輸出  スケジュール</a></li>
									<li class="list-group-item"><a href="#">輸入 スケジュール</a></li>
									<li class="list-group-item"><a href="#">スケジュール</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-4 inner-side-md">
						<div class="panel panel-default">
							<h3><a data-toggle="tooltip" title="Search Schedule"> 各種情報</a></h3>
							<div class="panel-body panel-inner-sm">
								<ul class="list-group">
									<li class="list-group-item"><a href="#">輸出  スケジュール</a></li>
									<li class="list-group-item"><a href="#">輸入 スケジュール</a></li>
									<li class="list-group-item"><a href="#">スケジュール</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 inner-side-md listing">
						<h2>最新ニュース</h2>
						<ul class="news">
							<li><a href="#">輸出  スケジュール</a></li>
							<li><a href="#">輸入 スケジュール</a></li>
							<li><a href="#">スケジュール</a></li>
							<li><a href="#">輸入 スケジュール</a></li>
							<li><a href="#">スケジュール</a></li>
						</ul>
						<a href="news.php" class="btn btn-primary btn-submit">その他のニュース</a>
						<img src="../assets/images/banner.jpg" alt="Eculine Hero Image" class="thumbnail img">
					</div>
				</div>
				<!-- <div class="row">
					<div class="col-sm-12 inner-side-md">
						<h2>今後の予定</h2>
						<div class="bs-example bs-example-tabs mb30">
							<ul id="myTab" class="nav nav-tabs">
								<li class="active"><a href="#home-tab" data-toggle="tab">All Schedule</a></li>
								<li><a href="#profile" data-toggle="tab">Filter Schedule 1</a></li>
								<li><a href="#profile2" data-toggle="tab">Filter Schedule 2</a></li>
								<li><a href="#profile3" data-toggle="tab">Filter Schedule 3</a></li>
							</ul>
							<div id="myTabContent" class="tab-content">
								<div class="tab-pane fade active in" id="home-tab">
									<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered listing">
										<thead>
											<tr role="row">
												<th>Date Added</th>
												<th>Schedule Name</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><span>10-20-2015</span></td>
												<td>
													<div class="media">
														<div class="pull-left media-middle">
															<a href="#">
																<img src="http://placehold.it/40x40">
															</a>
														</div>
														<div class="media-body">
															<h4 class="media-heading">Tokyo / Yokohama</h4>
														</div>
													</div>
												</td>
												<td>
													<a class="btn btn-danger" href="#">
														<i class="fa fa-file-pdf-o"></i>
														Pdf
													</a>
													<a class="btn btn-success" href="#">
														<i class="fa fa-file-excel-o"></i>
														Excel
													</a>
												</td>
											</tr>
											<tr>
												<td><span>10-20-2015</span></td>
												<td>
													<div class="media">
														<div class="pull-left media-middle">
															<a href="#">
																<img src="http://placehold.it/40x40">
															</a>
														</div>
														<div class="media-body">
															<h4 class="media-heading">Tokyo / Yokohama</h4>
														</div>
													</div>
												</td>
												<td>
													<a class="btn btn-danger" href="#">
														<i class="fa fa-file-pdf-o"></i>
														Pdf
													</a>
													<a class="btn btn-success" href="#">
														<i class="fa fa-file-excel-o"></i>
														Excel
													</a>
												</td>
											</tr>
											<tr>
												<td><span>10-20-2015</span></td>
												<td>
													<div class="media">
														<div class="pull-left media-middle">
															<a href="#">
																<img src="http://placehold.it/40x40">
															</a>
														</div>
														<div class="media-body">
															<h4 class="media-heading">Tokyo / Yokohama</h4>
														</div>
													</div>
												</td>
												<td>
													<a class="btn btn-danger" href="#">
														<i class="fa fa-file-pdf-o"></i>
														Pdf
													</a>
													<a class="btn btn-success" href="#">
														<i class="fa fa-file-excel-o"></i>
														Excel
													</a>
												</td>
											</tr>
											<tr>
												<td><span>10-20-2015</span></td>
												<td>
													<div class="media">
														<div class="pull-left media-middle">
															<a href="#">
																<img src="http://placehold.it/40x40">
															</a>
														</div>
														<div class="media-body">
															<h4 class="media-heading">Tokyo / Yokohama</h4>
														</div>
													</div>
												</td>
												<td>
													<a class="btn btn-danger" href="#">
														<i class="fa fa-file-pdf-o"></i>
														Pdf
													</a>
													<a class="btn btn-success" href="#">
														<i class="fa fa-file-excel-o"></i>
														Excel
													</a>
												</td>
											</tr>
											<tr>
												<td><span>10-20-2015</span></td>
												<td>
													<div class="media">
														<div class="pull-left media-middle">
															<a href="#">
																<img src="http://placehold.it/40x40">
															</a>
														</div>
														<div class="media-body">
															<h4 class="media-heading">Tokyo / Yokohama</h4>
														</div>
													</div>
												</td>
												<td>
													<a class="btn btn-danger" href="#">
														<i class="fa fa-file-pdf-o"></i>
														Pdf
													</a>
													<a class="btn btn-success" href="#">
														<i class="fa fa-file-excel-o"></i>
														Excel
													</a>
												</td>
											</tr>
											<tr>
												<td><span>10-20-2015</span></td>
												<td>
													<div class="media">
														<div class="pull-left media-middle">
															<a href="#">
																<img src="http://placehold.it/40x40">
															</a>
														</div>
														<div class="media-body">
															<h4 class="media-heading">Tokyo / Yokohama</h4>
														</div>
													</div>
												</td>
												<td>
													<a class="btn btn-danger" href="#">
														<i class="fa fa-file-pdf-o"></i>
														Pdf
													</a>
													<a class="btn btn-success" href="#">
														<i class="fa fa-file-excel-o"></i>
														Excel
													</a>
												</td>
											</tr>
											<tr>
												<td><span>10-20-2015</span></td>
												<td>
													<div class="media">
														<div class="pull-left media-middle">
															<a href="#">
																<img src="http://placehold.it/40x40">
															</a>
														</div>
														<div class="media-body">
															<h4 class="media-heading">Tokyo / Yokohama</h4>
														</div>
													</div>
												</td>
												<td>
													<a class="btn btn-danger" href="#">
														<i class="fa fa-file-pdf-o"></i>
														Pdf
													</a>
													<a class="btn btn-success" href="#">
														<i class="fa fa-file-excel-o"></i>
														Excel
													</a>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="tab-pane" id="profile">
									<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered listing">
										<thead>
											<tr role="row">
												<th>Date Added</th>
												<th>Schedule Name</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><span>10-20-2015</span></td>
												<td>
													<div class="media">
														<div class="pull-left media-middle">
															<a href="#">
																<img src="http://placehold.it/40x40">
															</a>
														</div>
														<div class="media-body">
															<h4 class="media-heading">Tokyo / Yokohama</h4>
														</div>
													</div>
												</td>
												<td>
													<a class="btn btn-danger" href="#">
														<i class="fa fa-file-pdf-o"></i>
														Pdf
													</a>
													<a class="btn btn-success" href="#">
														<i class="fa fa-file-excel-o"></i>
														Excel
													</a>
												</td>
											</tr>
											<tr>
												<td><span>10-20-2015</span></td>
												<td>
													<div class="media">
														<div class="pull-left media-middle">
															<a href="#">
																<img src="http://placehold.it/40x40">
															</a>
														</div>
														<div class="media-body">
															<h4 class="media-heading">Tokyo / Yokohama</h4>
														</div>
													</div>
												</td>
												<td>
													<a class="btn btn-danger" href="#">
														<i class="fa fa-file-pdf-o"></i>
														Pdf
													</a>
													<a class="btn btn-success" href="#">
														<i class="fa fa-file-excel-o"></i>
														Excel
													</a>
												</td>
											</tr>
											<tr>
												<td><span>10-20-2015</span></td>
												<td>
													<div class="media">
														<div class="pull-left media-middle">
															<a href="#">
																<img src="http://placehold.it/40x40">
															</a>
														</div>
														<div class="media-body">
															<h4 class="media-heading">Tokyo / Yokohama</h4>
														</div>
													</div>
												</td>
												<td>
													<a class="btn btn-danger" href="#">
														<i class="fa fa-file-pdf-o"></i>
														Pdf
													</a>
													<a class="btn btn-success" href="#">
														<i class="fa fa-file-excel-o"></i>
														Excel
													</a>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="panel panel-info">
								<div class="panel-heading">
									<h3><small>Port from:</small> DALIAN</h3>
								</div>
								<div class="panel-body">
									<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered listing">
										<thead>
											<tr>
												<th>Destination</th>
												<th>Schedule Detail</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Yokohama</td>
												<td>
													<ul class="nav navbar">
														<li><a class="btn btn-danger" href="#"><i class="fa fa-file-pdf-o"></i> November 2015</a></li>
													</ul>
												</td>
											</tr>
											<tr>
												<td>Nagoya</td>
												<td>
													<ul class="nav navbar">
														<li><a class="btn btn-danger" href="#"><i class="fa fa-file-pdf-o"></i> November 2015</a></li>
														<li><a class="btn btn-danger" href="#"><i class="fa fa-file-pdf-o"></i> December 2015</a></li>
													</ul>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								
							</div>
							<div class="pull-left call-to-action__button">
									<a href="schedule2.php" class="btn btn-primary btn-submit">More Schedule</a>
							</div>
							<div class="pull-left call-to-action__button">
								<a href="schedule2.php" class="btn btn-primary btn-submit">More News</a>
							</div>
							<img src="../assets/images/banner.jpg" alt="Eculine Hero Image" class="thumbnail img">
						</div>
					</div>
				</div> -->
				</div>
				<?php include "inc.sidebar.php";?>
			</div>
		</section>
		</main>
		<?php include "inc.footer.php";?>