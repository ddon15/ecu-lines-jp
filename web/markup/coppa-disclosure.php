<?php include "inc.header2.php";?>
			<section id="maincontent" class="content-body">
				<div class="container">
					<div class="row">
						<div class="col-sm-11 center-block text-center">
							<h1>COPPA Disclosure</h1>
						</div>
						<div class="col-sm-11 center-block">
							<div class="row">

<p>Thank you for using the Behavior World website (“Site”) and any associated applications (“Platform”). We try to make this disclosure easy to understand so that you are informed as to your rights and our responsibilities under COPPA. This disclosure should be read in conjunction with our Privacy Policy and Terms of Service. If you have any questions regarding this disclosure please contact us at <a href="mailto:support@behaviorworld.net">support@behaviorworld.net</a>.</p>
<h4><strong>1. What is COPPA?</strong></h4>

<p>COPPA stands for the Children’s Online Privacy Protection Act, which was enacted by Congress in 1998 and enforced by the Federal Trade Commission (FTC). The primary goal of COPPA is to place parents in control over what information is collected from their young children online or via mobile applications. COPPA applies to any personal information collected by online services and commercial websites for children under the age of 13. Personal information as defined by COPPA includes but is not limited to, names, ages, photos, email addresses, and birth dates. Websites and applications that fall under the purview of COPPA must do the following:</p>
<ul>
<li>Post a clear and comprehensive online privacy policy describing their information practices&nbsp;for personal information collected online from children;</li>
<li>Provide direct notice to parents and obtain verifiable parental consent, with limited&nbsp;exceptions, before collecting personal information online from children;</li>
<li>Give parents the choice of consenting to the operator’s collection and internal use of a child’s&nbsp;information, but prohibiting the operator from disclosing that information to third parties;</li>
<li>Provide parents access to their child’s personal information to review and/or have the&nbsp;information deleted;</li>
<li>Give parents the opportunity to prevent further use or online collection of a child’s personal&nbsp;information;</li>
<li>Maintain the confidentiality, security, and integrity of information they collect from&nbsp;children, including taking reasonable steps to release such information only to parties&nbsp;capable of maintaining its confidentiality and security; and</li>
<li>Retain personal information collected online from a child for only as long as is necessary to&nbsp;fulfill the purpose for which it was collected and delete the information using reasonable&nbsp;measures to protect against its unauthorized access or use.</li>
</ul>
<p>This disclosure explains how we intend to comply with COPPA and the guidelines above.</p>
<h4><strong>2. Privacy Policy</strong></h4>
<p>The Behavior World Privacy Policy is available for viewing through the Behavior World Site and Platform. Our Privacy Policy explains how we collect information, how we use the information collected, how we secure the information, who we share your information with, and your options for modifying and deleting any information collected. The privacy policy applies to a parent’s information and as well as your child’s information. If you have any questions regarding our information collection practices please contact us.</p>
<h4><strong>3. Parental Consent</strong></h4>
<p>Behavior World will never ask children under 13 to directly submit any personably identifiable information to the Platform. However, we may ask parents or guardians to submit information on behalf of a child to improve the functionality of the Platform. Please be aware that Behavior World will never ask for more information than is necessary to allow for the Platform to function properly.</p>
<p style="padding-left: 30px;"><strong><span style="text-decoration: underline;">Parents</span></strong></p>
<p>When a child’s information is submitted to our Platform, an email is sent to the parent’s address listed within the Platform which notifies the parents about our information collection practices. The email along with this disclosure and our Privacy Policy explains to parents their options regarding their child’s information.</p>
<p style="padding-left: 30px;"><strong><span style="text-decoration: underline;">Teachers</span></strong></p>
<p>We request that all teachers notify parents when they decide to use the Platform for the children in their class. When teachers submit a child’s information, they will have the option to contact parents via email or to send home permission slips. When Teachers download the Platform, they must be acting on behalf of their educational institution. Additionally, the use of the Platform should be under the educational institution’s direction and consent.</p>
<h4><strong>4. Information We Collect From Children, How We Use It, and How and When We Communicate With Parents</strong></h4>
<p>Behavior World offers a Platform where parents and teachers can track and reward their children’s and student’s behavior. Through the Platform we may collect their name, photo, and likeness. In any instance that we collect personal information from a child, we will retain that information only so long as reasonably necessary to fulfill the activity request or allow the child to continue to participate in the activity, and ensure the security of our users and our services, or as required by law. In the event we discover we have collected information from a child in a manner inconsistent with COPPA’s requirements, we will notify you.</p>
<h4><strong>5. Parental Choices and Controls</strong></h4>
<p>At any time, parents can refuse to permit us to collect any personal information from their children in association with the Platform. Parents can request that we delete any information that we have collected from their child via the Platform. Please be aware that a request to delete records may lead to a termination of behavior tracking services or access to the Platform.</p>
<p>If you wish for any of your child’s information to be removed or deleted, please contact us via email at <a href="mailto:support@behaviorworld.net">support@behaviorworld.net</a> or by mail at:</p>
<p>COPPA Agent<br>
10736 Jefferson Blvd. #367<br>
Culver, CA 90230<br>
United States</p>
<p>If you correspond with us via email or mail, please include identifying information related to the child and account that is associated with such information. In order to protect a child’s safety and security we may take steps to verify your identity and parental authority.</p>
	
							
	
		</div>
						</div>
					</div><!-- /.row -->
				</div><!-- /.container -->
			</section>
		</main>
<?php include "inc.footer.php";?>
		