<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Meta -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Eculine</title>
		<!-- Bootstrap Core CSS -->
		<link href="../assets/css/eculine.css" rel="stylesheet">
		<!-- HTML5 elements and media queries Support for IE8 : HTML5 shim and Respond.js -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
		<!-- Favicon -->
		<link rel="apple-touch-icon-precomposed" sizes="57x57" href="assets/images/apple-touch-icon-57x57.png" />
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/images/apple-touch-icon-114x114.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/images/apple-touch-icon-72x72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/images/apple-touch-icon-144x144.png" />
		<link rel="apple-touch-icon-precomposed" sizes="60x60" href="assets/images/apple-touch-icon-60x60.png" />
		<link rel="apple-touch-icon-precomposed" sizes="120x120" href="assets/images/apple-touch-icon-120x120.png" />
		<link rel="apple-touch-icon-precomposed" sizes="76x76" href="assets/images/apple-touch-icon-76x76.png" />
		<link rel="apple-touch-icon-precomposed" sizes="152x152" href="assets/images/apple-touch-icon-152x152.png" />
		<link rel="icon" type="image/png" href="assets/images/favicon-196x196.png" sizes="196x196" />
		<link rel="icon" type="image/png" href="assets/images/favicon-96x96.png" sizes="96x96" />
		<link rel="icon" type="image/png" href="assets/images/favicon-32x32.png" sizes="32x32" />
		<link rel="icon" type="image/png" href="assets/images/favicon-16x16.png" sizes="16x16" />
		<link rel="icon" type="image/png" href="assets/images/favicon-128.png" sizes="128x128" />
		<meta name="application-name" content="&nbsp;"/>
		<meta name="msapplication-TileColor" content="#FFFFFF" />
		<meta name="msapplication-TileImage" content="assets/images/mstile-144x144.png" />
		<meta name="msapplication-square70x70logo" content="assets/images/mstile-70x70.png" />
		<meta name="msapplication-square150x150logo" content="assets/images/mstile-150x150.png" />
		<meta name="msapplication-wide310x150logo" content="assets/images/mstile-310x150.png" />
		<meta name="msapplication-square310x310logo" content="assets/images/mstile-310x310.png" />
	</head>
	<body>
		<header class="container">

		<div class="bs-example">
    <nav role="navigation" class="navbar">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand"><img src="../assets/images/logo.png" class="logo" alt="" title="Ecu-line"></a>
        </div>
        <!-- Collection of nav links, forms, and other content for toggling -->
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav menu-ecu">
            	<li><a href="index.php" title="Home">ホーム</a></li>
				<li><a href="schedule.php" title="Schedule">スケジュール</a></li>
				<li><a href="http://obs.as.eculine.net/" title="Booking" target="_blank">ブッキング</a></li>
				<li><a href="http://webapplications2.as.eculine.net/vad/" title="Exchange Rate and Vessel Movement" target="_blank">換算レートと本船動静</a></li>
				<li><a href="surcharge.php" title="Surcharge">サーチャージ</a></li>
				<li><a href="aboutus.php" title="Company Profile">各国情報</a></li>
				<li><a href="contactus.php" title="Contact Information">接触</a></li>
            </ul>

        </div>
    </nav>
</div>

	
		</header>
		<main>