<?php include "inc.header.php";?>
			<section class="container main">
				<div class="row">
					<div class="col-sm-8 inner-side-md maincontent">
						<!-- <h4>
							The World's NVOCC Market Leader ! 200+ Offices in over 90 countries.
						</h4> -->

						<h1>
							スケジュール 
						</h1>
						<hr>
<!-- 
						<div class="col-sm-10 col-sm-offset-1">
							<div class="well login">
								<h4>Search Schedule</h4>
								<form class="form-horizontal">
									<div class="row">
									    <div class="col-sm-6">
									    	<label for="from">From</label>
											<input type="text" class="form-control" placeholder="e.g Tokyo">
										</div>
									
									    <div class="col-sm-6">
									    	<label for="from">To</label>
											<input type="text" class="form-control" placeholder="e.g Tokyo">
										</div>
									</div>
									<br>
									<div class="form-group">
										<div class="col-sm-12">
											<button type="submit" class="btn btn-primary">提出します</button>
										</div>
									</div>
								</form>
							</div>
						</div> -->

												<ol class="breadcrumb">
						<li><a href="schedule2.php">Schedule Listing</a></li>
						<li class="active">Asia</li>
						</ol>

						<h2>Asia</h2>
						<div class="panel panel-info">
							<div class="panel-heading">
								<h3><small>Port from:</small> DALIAN</h3>
							</div>
							<div class="panel-body">
								<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered listing">
									<thead>
										<tr>
											<th>Destination</th>
											<th>Schedule Detail</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Yokohama</td>
											<td>
												<ul class="nav navbar">
													<li><a class="btn btn-success" href="#"><i class="fa fa-file-excel-o"></i> November 2015</a></li>
												</ul>
											</td>
										</tr>
										<tr>
											<td>Nagoya</td>
											<td>
												<ul class="nav navbar">
													<li><a class="btn btn-success" href="#"><i class="fa fa-file-excel-o"></i> November 2015</a></li>
													<li><a class="btn btn-success" href="#"><i class="fa fa-file-excel-o"></i> December 2015</a></li>
												</ul>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="panel panel-info">
							<div class="panel-heading">
								<h3><small>Port from:</small> SHANGHAI</h3>
							</div>
							<div class="panel-body">
								<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered listing">
									<thead>
										<tr>
											<th>Destination</th>
											<th>Schedule Detail</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Yokohama</td>
											<td>
												<ul class="nav navbar">
													<li><a class="btn btn-success" href="#"><i class="fa fa-file-excel-o"></i> November 2015</a></li>
													<li><a class="btn btn-success" href="#"><i class="fa fa-file-excel-o"></i> December 2015</a></li>
												</ul>
											</td>
										</tr>
										<tr>
											<td>Nagoya</td>
											<td>
												<ul class="nav navbar">
													<li><a class="btn btn-success" href="#"><i class="fa fa-file-excel-o"></i> November 2015</a></li>
												</ul>
											</td>
										</tr>
										<tr>
											<td>Kobe</td>
											<td>
												<ul class="nav navbar">
													<li><a class="btn btn-success" href="#"><i class="fa fa-file-excel-o"></i> November 2015</a></li>
													<li><a class="btn btn-success" href="#"><i class="fa fa-file-excel-o"></i> December 2015</a></li>
												</ul>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<!-- <div class="bs-example bs-example-tabs mb30">
							<ul id="myTab" class="nav nav-tabs">
								<li class="active"><a href="#home-tab" data-toggle="tab">All Schedule</a></li>
								<li><a href="#profile" data-toggle="tab">Filter Schedule 1</a></li>
								<li><a href="#profile2" data-toggle="tab">Filter Schedule 2</a></li>
								<li><a href="#profile3" data-toggle="tab">Filter Schedule 3</a></li>
							</ul>
						</div> -->
						<!-- <div id="myTabContent" class="tab-content">
											<div class="tab-pane fade active in" id="home-tab">
												<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered listing">
					                      			<thead>
					                          			<tr role="row">
					                          				<th>Date Added</th>
					                          				<th>Schedule Name</th>
					                          				<th>Actions</th>
					                          			</tr>
					                      			</thead>   
					                      			<tbody>
					                       				<tr>
					                       					<td><span>10-20-2015</span></td>
					                       					<td>
								                            	<div class="media">
					  												<div class="pull-left media-middle">
																	    <a href="#">
																	      <img src="http://placehold.it/40x40">
																	    </a>
					  												</div>
																	<div class="media-body">
																	    <h4 class="media-heading">Tokyo / Yokohama</h4>
																	</div>
																</div>
								                            </td>
								                            <td>
					                                			<a class="btn btn-success" href="#">
					                                    			<i class="fa fa-file-excel-o"></i>  
					                                    			Pdf                                           
					                               				 </a>
								                                <a class="btn btn-success" href="#">
								                                    <i class="fa fa-file-excel-o"></i>  
								                                    Excel                                            
								                                </a>
					                           				</td>
					                       				</tr>
					                       				<tr>
					                       					<td><span>10-20-2015</span></td>
					                       					<td>
								                            	<div class="media">
					  												<div class="pull-left media-middle">
																	    <a href="#">
																	      <img src="http://placehold.it/40x40">
																	    </a>
					  												</div>
																	<div class="media-body">
																	    <h4 class="media-heading">Tokyo / Yokohama</h4>
																	</div>
																</div>
								                            </td>
								                            <td>
					                                			<a class="btn btn-success" href="#">
					                                    			<i class="fa fa-file-excel-o"></i>  
					                                    			Pdf                                           
					                               				 </a>
								                                <a class="btn btn-success" href="#">
								                                    <i class="fa fa-file-excel-o"></i>  
								                                    Excel                                            
								                                </a>
					                           				</td>
					                       				</tr>
					                       				<tr>
					                       					<td><span>10-20-2015</span></td>
					                       					<td>
								                            	<div class="media">
					  												<div class="pull-left media-middle">
																	    <a href="#">
																	      <img src="http://placehold.it/40x40">
																	    </a>
					  												</div>
																	<div class="media-body">
																	    <h4 class="media-heading">Tokyo / Yokohama</h4>
																	</div>
																</div>
								                            </td>
								                            <td>
					                                			<a class="btn btn-success" href="#">
					                                    			<i class="fa fa-file-excel-o"></i>  
					                                    			Pdf                                           
					                               				 </a>
								                                <a class="btn btn-success" href="#">
								                                    <i class="fa fa-file-excel-o"></i>  
								                                    Excel                                            
								                                </a>
					                           				</td>
					                       				</tr>
					                       				<tr>
					                       					<td><span>10-20-2015</span></td>
					                       					<td>
								                            	<div class="media">
					  												<div class="pull-left media-middle">
																	    <a href="#">
																	      <img src="http://placehold.it/40x40">
																	    </a>
					  												</div>
																	<div class="media-body">
																	    <h4 class="media-heading">Tokyo / Yokohama</h4>
																	</div>
																</div>
								                            </td>
								                            <td>
					                                			<a class="btn btn-success" href="#">
					                                    			<i class="fa fa-file-excel-o"></i>  
					                                    			Pdf                                           
					                               				 </a>
								                                <a class="btn btn-success" href="#">
								                                    <i class="fa fa-file-excel-o"></i>  
								                                    Excel                                            
								                                </a>
					                           				</td>
					                       				</tr>
					                       				<tr>
					                       					<td><span>10-20-2015</span></td>
					                       					<td>
								                            	<div class="media">
					  												<div class="pull-left media-middle">
																	    <a href="#">
																	      <img src="http://placehold.it/40x40">
																	    </a>
					  												</div>
																	<div class="media-body">
																	    <h4 class="media-heading">Tokyo / Yokohama</h4>
																	</div>
																</div>
								                            </td>
								                            <td>
					                                			<a class="btn btn-success" href="#">
					                                    			<i class="fa fa-file-excel-o"></i>  
					                                    			Pdf                                           
					                               				 </a>
								                                <a class="btn btn-success" href="#">
								                                    <i class="fa fa-file-excel-o"></i>  
								                                    Excel                                            
								                                </a>
					                           				</td>
					                       				</tr>
					                       				<tr>
					                       					<td><span>10-20-2015</span></td>
					                       					<td>
								                            	<div class="media">
					  												<div class="pull-left media-middle">
																	    <a href="#">
																	      <img src="http://placehold.it/40x40">
																	    </a>
					  												</div>
																	<div class="media-body">
																	    <h4 class="media-heading">Tokyo / Yokohama</h4>
																	</div>
																</div>
								                            </td>
								                            <td>
					                                			<a class="btn btn-success" href="#">
					                                    			<i class="fa fa-file-excel-o"></i>  
					                                    			Pdf                                           
					                               				 </a>
								                                <a class="btn btn-success" href="#">
								                                    <i class="fa fa-file-excel-o"></i>  
								                                    Excel                                            
								                                </a>
					                           				</td>
					                       				</tr>
					                       				<tr>
					                       					<td><span>10-20-2015</span></td>
					                       					<td>
								                            	<div class="media">
					  												<div class="pull-left media-middle">
																	    <a href="#">
																	      <img src="http://placehold.it/40x40">
																	    </a>
					  												</div>
																	<div class="media-body">
																	    <h4 class="media-heading">Tokyo / Yokohama</h4>
																	</div>
																</div>
								                            </td>
								                            <td>
					                                			<a class="btn btn-success" href="#">
					                                    			<i class="fa fa-file-excel-o"></i>  
					                                    			Pdf                                           
					                               				 </a>
								                                <a class="btn btn-success" href="#">
								                                    <i class="fa fa-file-excel-o"></i>  
								                                    Excel                                            
								                                </a>
					                           				</td>
					                       				</tr>
					                        		</tbody>
		                        				</table>
											</div>
											<div class="tab-pane" id="profile">
												<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered listing">
					                      			<thead>
					                          			<tr role="row">
					                          				<th>Date Added</th>
					                          				<th>Schedule Name</th>
					                          				<th>Actions</th>
					                          			</tr>
					                      			</thead>   
					                      			<tbody>
					                       				<tr>
					                       					<td><span>10-20-2015</span></td>
					                       					<td>
								                            	<div class="media">
					  												<div class="pull-left media-middle">
																	    <a href="#">
																	      <img src="http://placehold.it/40x40">
																	    </a>
					  												</div>
																	<div class="media-body">
																	    <h4 class="media-heading">Tokyo / Yokohama</h4>
																	</div>
																</div>
								                            </td>
								                            <td>
					                                			<a class="btn btn-success" href="#">
					                                    			<i class="fa fa-file-excel-o"></i>  
					                                    			Pdf                                           
					                               				 </a>
								                                <a class="btn btn-success" href="#">
								                                    <i class="fa fa-file-excel-o"></i>  
								                                    Excel                                            
								                                </a>
					                           				</td>
					                       				</tr>
					                       				<tr>
					                       					<td><span>10-20-2015</span></td>
					                       					<td>
								                            	<div class="media">
					  												<div class="pull-left media-middle">
																	    <a href="#">
																	      <img src="http://placehold.it/40x40">
																	    </a>
					  												</div>
																	<div class="media-body">
																	    <h4 class="media-heading">Tokyo / Yokohama</h4>
																	</div>
																</div>
								                            </td>
								                            <td>
					                                			<a class="btn btn-success" href="#">
					                                    			<i class="fa fa-file-excel-o"></i>  
					                                    			Pdf                                           
					                               				 </a>
								                                <a class="btn btn-success" href="#">
								                                    <i class="fa fa-file-excel-o"></i>  
								                                    Excel                                            
								                                </a>
					                           				</td>
					                       				</tr>
					                       				<tr>
					                       					<td><span>10-20-2015</span></td>
					                       					<td>
								                            	<div class="media">
					  												<div class="pull-left media-middle">
																	    <a href="#">
																	      <img src="http://placehold.it/40x40">
																	    </a>
					  												</div>
																	<div class="media-body">
																	    <h4 class="media-heading">Tokyo / Yokohama</h4>
																	</div>
																</div>
								                            </td>
								                            <td>
					                                			<a class="btn btn-success" href="#">
					                                    			<i class="fa fa-file-excel-o"></i>  
					                                    			Pdf                                           
					                               				 </a>
								                                <a class="btn btn-success" href="#">
								                                    <i class="fa fa-file-excel-o"></i>  
								                                    Excel                                            
								                                </a>
					                           				</td>
					                       				</tr>
					                        		</tbody>
		                        				</table>
											</div>
						</div> -->
					</div>
					<?php include "inc.sidebar.php";?>
				</div>
			</section>
		</main>
<?php include "inc.footer.php";?>
		